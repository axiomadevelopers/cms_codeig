-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 08-01-2019 a las 18:31:10
-- Versión del servidor: 5.7.21
-- Versión de PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_cms_ci`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

DROP TABLE IF EXISTS `categorias`;
CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `descripcion`, `estatus`) VALUES
(1, 'SLIDER', 0),
(2, 'marcas', 0),
(3, 'quienes_somos', 1),
(4, 'noticias', 1),
(5, 'Detalles negocios', 1),
(6, 'imagenes', 1),
(7, 'servicios', 1),
(8, 'Portafolio', 1),
(9, 'Clientes', 1),
(10, '', 2),
(11, '', 2),
(12, '', 2),
(13, '', 2),
(14, '', 2),
(15, '', 2),
(16, '', 2),
(17, '', 2),
(18, 'PRUEBA DE DESCRIPCION', 2),
(19, 'PRUEBA SLIDER DOS', 1),
(20, 'Nosotros', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `descripcion`, `id_idioma`, `estatus`) VALUES
(2, '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum libero libero, quis iaculis est venenatis quis. Quisque sodales id ligula eget gravida. Nulla scelerisque arcu at eleifend maximus. Cras cursus, quam at lacinia iaculis, ex mauris pretium nibh, sed cursus urna augue vitae nisi. Nulla nulla augue, placerat a magna vel, semper placerat turpis. Ut ut velit ornare, mollis odio nec, fringilla tortor. Praesent vitae ligula bibendum, accumsan ante sed, pretium magna.</span></p>', 1, 1),
(3, '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum libero libero, quis iaculis est venenatis quis. Quisque sodales id ligula eget gravida. Nulla scelerisque arcu at eleifend maximus. Cras cursus, quam at lacinia iaculis, ex mauris pretium nibh, sed cursus urna augue vitae nisi. Nulla nulla augue, placerat a magna vel, semper placerat turpis. Ut ut velit ornare, mollis odio nec, fringilla tortor. Praesent vitae ligula bibendum, accumsan ante sed, pretium magna.</span></p>', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

DROP TABLE IF EXISTS `contactos`;
CREATE TABLE IF NOT EXISTS `contactos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` text NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mensaje` text NOT NULL,
  `razon` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`id`, `nombres`, `telefono`, `email`, `mensaje`, `razon`) VALUES
(12, 'miguel', '04122473595', 'mporro@cantv.net', 'mensaje de prueba de contacto', ''),
(13, 'miguel', '04122473595', 'mporro@cantv.net', 'mensaje de prueba de contacto', ''),
(14, 'prueba', '04124567788', 'prueba@gmail.com', 'Prueba', ''),
(15, 'prueba', '04124567788', 'prueba@gmail.com', 'Prueba', ''),
(16, 'prueba', '04167485648', 'asdf@gmail.com', 'prueba', ''),
(17, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(18, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(19, 'GIanni', '04167485648', 'gianni.d.santucci@gmail.com', 'Prueba de envio', ''),
(20, 'GIanni', '04167485648', 'gianni.d.santucci@gmail.com', 'Prueba de envio', ''),
(21, 'GIanni', '04167485648', 'gianni.d.santucci@gmail.com', 'Prueba de envio', ''),
(22, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(23, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(24, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(25, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(26, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(27, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(28, 'Carlos Herrera', '04126372219', 'carlosdhm6@gmail.com', 'Prueba', ''),
(29, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(30, 'Carlos Herrera', '04126372219', 'carlosdhm6@gmail.com', 'Prueba', ''),
(31, 'Carlos Herrera', '04126372219', 'carlosdhm6@gmail.com', 'Prueba', ''),
(32, 'Prueba', '04167485648', 'prueba@gmail.com', 'Prueba', ''),
(33, 'prueba', '04167485648', 'asdf@gmail.com', 'prueba', ''),
(34, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(35, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(36, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(37, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(38, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(39, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(40, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(41, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(42, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(43, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(44, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(45, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(46, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(47, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(48, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(49, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(50, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(51, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(52, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(53, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(54, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(55, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(56, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(57, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(58, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(59, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(60, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(61, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(62, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(63, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(64, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(65, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(66, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(67, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(68, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba', ''),
(69, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(70, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(71, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(72, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(73, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(74, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(75, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(76, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(77, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(78, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(79, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(80, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(81, 'prueba', '04167485648', 'prueba@gmail.co', 'Prueba', ''),
(82, 'prueba', '02128723999', 'prueba@gmail.com', 'prueba', ''),
(83, 'prueba', '02128723999', 'prueba@gmail.com', 'prueba', ''),
(84, 'prueba de email', '02125555555', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(85, 'Gianni santucci', '04167485648', 'prueba@gmail.com', 'prueba de mensaje', ''),
(86, 'prueba', '02126799999', 'prueba@gmail.com', 'prueba', ''),
(87, 'Gianni', '04167888888', 'paginaweb@itssca.net', 'prueba', ''),
(88, 'Gianni Santucci', '02127777777', 'gianni.d.santucci@gmail.com', 'Prueba de que la web de itssca si envia emails!', ''),
(89, 'Prueba', '02120000000', 'prueba@gmail.com', 'prueba desde sección contactanos', ''),
(90, 'prueba', '02123455555', 'prueba@gmail.com', 'prueba', ''),
(91, 'prueba viernes', '04167485648', 'prueba_viernes@gmail.com', 'Prueba de mensaje viernes 17112017', ''),
(92, 'Gianni', '04165674455', 'Gianni@gmail.com', 'Prueba', ''),
(93, 'carlos', '04126372219', 'carlos.herrera@itssca.net', 'sssss', ''),
(94, 'carlos', '04126372219', 'carlos.herrera@itssca.net', 'carlos hod', ''),
(95, 'carlos', '04126372219', 'carlosdhm6@gmail.com', 'ndsnaod', ''),
(96, 'carlos', '04126372219', 'carlosdhm6@gmail.com', 'noisanodnasd', ''),
(97, 'carlos', '04126372219', 'carlosdhm6@gmail.com', 'ndsadnald', ''),
(98, 'Prueba', '04164444444', 'prueba@gmail.com', 'Prueba de envio de email gianni santucci 15:52 pm', ''),
(99, 'carlos', '04125324219', 'carlosdhm6@gmail.como', 'sssss', ''),
(100, 'Prueba', '04167485648', 'prueba.gianni@gmail.com', 'Prueba gianni santucci', ''),
(101, 'Prueba', '04167485648', 'prueba.gianni@gmail.com', 'Prueba gianni santucci', ''),
(102, 'Prueba', '04167485648', 'prueba.gianni@gmail.com', 'Prueba gianni santucci', ''),
(103, 'Prueba', '04167485648', 'prueba.gianni@gmail.com', 'Prueba gianni santucci', ''),
(104, 'Prueba', '04164444444', 'prueba@gmail.com', 'prueba', ''),
(105, 'prueba', '04169777777', 'pruyeba@gmail.com', 'prueba', ''),
(106, 'carlos herrera', '04126372219', 'carlosdhm6@gmail.com', 'midsdbaosdad', ''),
(107, 'gustavo', '04126379299', 'gustavo.w@hotmail.com', 'qwerty', ''),
(108, 'prueba gianni', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba gianni', ''),
(109, 'prueba gianni', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba gianni', ''),
(110, 'prueba', '04167485648', 'prueba.gianni@gmail.com', 'prueba', ''),
(111, 'prueba', '04167485648', 'prueba@gmail.com', 'prueba de mensaje', ''),
(112, 'Gianni', '04167485648', 'gianni.santucci@hotmail.com', 'prueba', ''),
(113, 'Gianni', '04167485648', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(114, 'Gianni Santucci', '04124555555', 'prueba@gmail.com', 'prueba', ''),
(115, 'asdwnoq', '04124567890', 'carlos@algo.com', 'sdabiasndkc\nEnvio 5', ''),
(116, 'asdwnoq', '04124567890', 'carlos@algo.com', 'sdabiasndkc\nEnvio 5', ''),
(117, 'maria vinals', '04246789024', 'maria@gmail.com', 'misterdnsodnso\nenvio 6', ''),
(118, 'carlos', '04123456789', 'yupi@itssca.net', 'wertyuiomnbvfrtyujm', ''),
(119, 'Trino Gonzlez', '04249383987', 'trinoinocente01@gmail.com', 'Muy buenas, quiero saber como contrato o compro la aplicación para usar el servicio P2P', ''),
(120, 'Gianni Santucci', '04167485648', 'gianni.d.santucci@gmail.com', 'Prueba de envío página web', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos_empleo`
--

DROP TABLE IF EXISTS `contactos_empleo`;
CREATE TABLE IF NOT EXISTS `contactos_empleo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(400) NOT NULL,
  `email` varchar(400) NOT NULL,
  `area` varchar(400) NOT NULL,
  `mensaje` text NOT NULL,
  `cv` text NOT NULL,
  `telefono` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contactos_empleo`
--

INSERT INTO `contactos_empleo` (`id`, `nombres`, `email`, `area`, `mensaje`, `cv`, `telefono`) VALUES
(36, 'gianni', 'gianni.d.santucci@gmail.com', 'prueba', 'prueba de registro', '../administrador/site_media/images/archivos/cv/cv_gianni.pdf', '04167485648'),
(47, 'registro', 'registro@gmail.com', 'prueba', 'prueba', '../administrador/site_media/images/archivos/cv/cv_registro.pdf', '04167485648'),
(48, 'registrodos', 'registrodos@gmail.com', 'prueba', 'prueba', '../administrador/site_media/images/archivos/cv/cv_registrodos.pdf', '04167485648'),
(49, 'Miguel', 'mporro@cantv.net', 'desarrollo', 'mensaje de prueba', '../administrador/site_media/images/archivos/cv/cv_Miguel.pdf', '04122473595'),
(50, 'Gianni santucci', 'asdf@gmail.com', 'prueba', 'esto es una prueba de envío de email desde itssca.net', '../administrador/site_media/images/archivos/cv/cv_Gianni_santucci.pdf', '04129877777'),
(51, 'Gianni Santucci', 'gianni.d.santucci@gmail.com', 'prueba', 'Prueba de envio de email. desde la web.', '../administrador/site_media/images/archivos/cv/cv_Gianni_Santucci.pdf', '04126666666'),
(52, 'Prueba de cv', 'cv@gmail.com', 'Prueba', 'Prueba de cv desde seccion contactanos', '../administrador/site_media/images/archivos/cv/cv_Prueba_de cv.pdf', '02129999999'),
(53, 'Gustavo Ocanto', 'gjocanto@gmail.com', 'vendedor de', 'sos chevere', '../administrador/site_media/images/archivos/cv/cv_Gustavo_Ocanto.pdf', '02125455122'),
(54, 'Prueba de cv', 'prueba@gmail.com', 'Prueba', 'Prueba adjuntando archivos', '../administrador/site_media/images/archivos/cv/cv_Prueba_de cv.pdf', '02123333333'),
(55, 'prueba', 'prueba@gmail.com', 'prueba', 'prueba', '../administrador/site_media/images/archivos/cv/cv_prueba.pdf', '02121444444'),
(56, 'prueba ultima del dia', 'asdf@gmail.com', 'prueba', 'prueba', '../administrador/site_media/images/archivos/cv/cv_prueba_ultima del dia.pdf', '04125555555'),
(57, 'Gianni Snatucci', 'prueba@gmail.com', 'prueba', 'Prueba de viernes 17-11-2017', '../administrador/site_media/images/archivos/cv/cv_Gianni_Snatucci.pdf', '04167415588'),
(58, 'Prueba', 'prueba@gmail.com', 'prueba', 'Prueba viernes 17 11 2014', '../administrador/site_media/images/archivos/cv/cv_Prueba.pdf', '04167485648'),
(59, 'Gianni Santucci', 'gianni.d.santucci@gmail.com', 'Prueba', 'Prueba de registro de cv página web', '../administrador/site_media/images/archivos/cv/cv_Gianni_Santucci.pdf', '04167485648'),
(60, 'Abimael Milano', 'abi_milano10@hotmail.com', 'Telecomunicaciones', 'Buenos días, me interesaría conocerlos mas a fondo y tener la oportunidad de crecer junto a la empre', '../administrador/site_media/images/archivos/cv/cv_Abimael_Milano.pdf', '04241752935'),
(61, 'Luis Eduardo Carpintero', 'luise2612@gmail.com', 'Consultor de redes y telecomunicaciones', 'Proactivo, creativo y disciplinado; Luis Carpintero es un consultor de redes que disfruta de encontrar soluciones creativas para redes complejas, utilizando las mejores prácticas. Con una comprensión rápida, solución y viabilidad en el aprendizaje de nuevas tecnologías', '../administrador/site_media/images/archivos/cv/cv_Luis_Eduardo Carpintero.pdf', '04241671246'),
(62, 'Luigi Toro', 'toroluigi123@gmail.com', 'soporte', 'Actualmente soy analista de soporte en venevision plus, me gustaría ingresar en una área relacionado', '../administrador/site_media/images/archivos/cv/cv_Luigi_Toro.pdf', '04140203600');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correos`
--

DROP TABLE IF EXISTS `correos`;
CREATE TABLE IF NOT EXISTS `correos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `correo` varchar(100) NOT NULL,
  `titulo` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `correos`
--

INSERT INTO `correos` (`id`, `correo`, `titulo`, `estatus`, `id_idioma`, `posicion`) VALUES
(1, 'ventas@itssca.net', 'Asistencia Comercial', 1, 1, 4),
(2, 'soporte@itssca.net', 'Asistencia Técnica', 1, 1, 1),
(3, 'mercadeo@itssca.net', 'Mercadeo', 1, 1, 2),
(4, 'rrhh@itssca.net', 'Empleos', 1, 1, 3),
(5, 'webmaster@itssca.net', 'Webmaster', 0, 1, 1),
(6, 'prueba@gmail.com', 'prueba', 0, 1, 2),
(7, 'support@itssca.net', 'Technical Support', 1, 2, 0),
(8, 'sales@itssca.net', 'Sales', 1, 2, 0),
(9, 'marketing@itssca.net', 'Marketing', 1, 2, 0),
(10, 'rrhh@itssca.net', 'Work with us', 1, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_tipo_negocio`
--

DROP TABLE IF EXISTS `detalle_tipo_negocio`;
CREATE TABLE IF NOT EXISTS `detalle_tipo_negocio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text NOT NULL,
  `descripcion1` text NOT NULL,
  `descripcion2` text NOT NULL,
  `descripcion3` text NOT NULL,
  `id_tipo_negocio` int(11) NOT NULL,
  `id_imagen` int(50) NOT NULL,
  `estatus` int(11) NOT NULL,
  `slug` text NOT NULL,
  `folleto` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_tipo_negocio`
--

INSERT INTO `detalle_tipo_negocio` (`id`, `titulo`, `descripcion1`, `descripcion2`, `descripcion3`, `id_tipo_negocio`, `id_imagen`, `estatus`, `slug`, `folleto`, `id_idioma`, `posicion`) VALUES
(3, 'SAMF', '<p>Es una plataforma de gestión de identidad ultra segura, de nueva generación , orientada al sector financiero. Autentica y gestiona claves estáticas y dinámicas, posee facilidades para autenticación positiva, es fácil de integrar con cualquier canal electrónico de atención de clientes del banco en un ambiente SOA. </p><p>Aplicado correctamente, SAMF fortalece la autenticación a través de los distintos canales tales como banca en línea, banca móvil, cajeros automáticos,&nbsp; Pagos Móviles P2P y P2B,&nbsp; call center y otro tipo de canales.&nbsp; SAMF es una solución de autenticación multicanal. Esta permite implementar mecanismos avanzados de&nbsp; autenticación usando múltiples factores de forma centralizada. </p><p>Permite la creación y administración de las políticas de autenticación de cada canal a través de una sola interfaz unificada de administración. Facilita la administración de perfiles y usuarios del sistema, incluye una poderosa herramienta forensica con registro, almacenamiento y visualización de logs de auditoria. </p>', '<h4 style=\"text-align: center;\"><br></h4><h4><ul style=\"color: rgb(118, 118, 118); font-size: 14px;\"><li><sup>Total cumplimiento de Resolución 641-10 en la República Bolivariana de Venezuela.</sup><br></li><li><sup>Administración centralizada del proceso de autenticación </sup></li><li><sup>Comunicación segura entre SAMF y Canales Fácil integración con canales electrónicos de atención al cliente</sup><br></li><li><sup>Escalabilidad ilimitada</sup><br></li><li><sup>Rápida adaptación a cambios de normativas y políticas</sup><br></li><li><sup>Poderosa herramienta de auditoria y forensica</sup></li></ul></h4>', '<h4 style=\"text-align: center;\"><br></h4>', 2, 24, 1, 'samf', '../administrador/site_media/images/archivos/folletos/folleto_SAMF.pdf', 1, 1),
(4, 'SERVICIOS DE SEGURIDAD ADMINISTRADA', '<h4><span style=\"text-align: justify;\"><sup>IT Security Solutions a través de su gerencia de Servicios Profesionales pone a su disposición un avanzado servicio de Seguridad Administrada con la finalidad de Administrar y Monitorear en tiempo real 7x24x365 su infraestructura de Telecomunicaciones y Seguridad.</sup></span><br></h4><br>', '<h4><strong>LOS SERVICIOS DE SEGURIDAD ADMINISTRADA PUEDEN SER EN LAS ÁREAS DE:&nbsp;</strong></h4><p style=\"text-align: justify; \">Monitoreo de Disponibilidad Administración y Monitoreo de Seguridad Detección de Vulnerabilidades</p><br>', '<h4 style=\"text-align: justify;\"><sup>Con este servicio, nuestros clientes se garantizan que su infraestructura va a ser monitoreada de forma pro-activa permitiéndoles enfocar sus esfuerzos a su ámbito de negocio.&nbsp;<br></sup></h4><h4 style=\"text-align: justify;\"><sup>Este servicio está especialmente diseñado para organizaciones que carecen de recursos humanos con experiencia y/o capacidades de monitoreo y seguridad, no disponen de suficiente personal, o no desean realizar gastos en soluciones de seguridad, bajo esta última premisa, ponemos a su disposición el servicio de Servicio Administrado de Seguridad Total, mediante el cual IT Security Solutions instala en las facilidades del cliente los sistemas necesarios los cuales contaran con pólizas de soporte post-venta por un canon mensual.</sup></h4><br>', 3, 19, 1, 'servicios-de-seguridad-administrada', '', 1, 0),
(5, 'SATM', '<blockquote><h4><span style=\"text-align: justify;\">Es una solución de monitoreo de seguridad proactiva para redes de cajeros automáticos, ayuda a mitigar el fraude electrónico, monitorea los ATM en forma permanente y alerta ante la presencia de actividades sospechosas.&nbsp;</span><br></h4></blockquote><blockquote><h4>SATM consiste de tres elemento básicos: OSSIM, S-Server, S-Agente</h4></blockquote><br>', '<p><h4 style=\"text-align: center;\"><strong>FUNCIONALIDADES</strong></h4><ul><li style=\"text-align: justify;\">Administración Centralizada&nbsp;<br></li></ul></p><p><ul><li style=\"text-align: justify;\">Monitoreo en Tiempo Real (HW y SW del ATM)<br></li><li style=\"text-align: justify;\">Recopilación de Logs de Auditoria<br></li><li style=\"text-align: justify;\">&nbsp;Auto Protección de Agente del ATM&nbsp;<br></li><li style=\"text-align: justify;\">Análisis de Vulnerabilidades Herramientas Forenses&nbsp;<br></li><li style=\"text-align: justify;\">Cumplimiento Normas PCI<br></li></ul></p>', '<h4 style=\"text-align: center;\"><span style=\"text-align: justify;\"><strong>Características Clave del SATM&nbsp;</strong></span></h4><p style=\"text-align: center; \"></p><ul><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Acceso seguro vía HTTPS&nbsp;</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Administración de Usuarios y Perfiles</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Logs de actividad de usuarios</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Análisis de Vulnerabilidades de los ATM</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Visor de Disponibilidad de ATM y Servicios Configuración de Acciones ante la presencia de actividad sospechosa&nbsp;</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Visor de Alarmas, Trafico, Riesgo y Eventos&nbsp;</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Reportes Forenses Edición de Directivas de Correlación Dashboard del Sistema</span><br></li></ul><p></p><br>', 2, 25, 1, 'satm', '../administrador/site_media/images/archivos/folletos/folleto_SATM.pdf', 1, 4),
(6, 'SOPORTE POST-VENTA Y ASISTENCIA', '<h4 style=\"text-align: left;\"><span style=\"text-align: justify; color: rgb(118, 118, 118); font-size: 14px; line-height: 23px;\">A través de nuestra Gerencia de Servicios Profesionales ponemos a su disposición servicios de soporte y asistencia técnica al software desarrollado por nuestra empresa, y a productos de terceras partes comercializados que incluyen soporte técnico de software, hardware, mantenimiento de equipos y reemplazo de equipos (RMA).</span><br></h4><p><strong>Características Clave del Servicio de Asistencia Técnica &nbsp;</strong> </p><ul><li>Soporte ilimitado a incidentes vía teléfono, email y website &nbsp; </li><li>El Cliente puede designar hasta 2 personas (coordinadores de soporte) que pueden contactar a nuestro personal para soporte y asistencia durante la vigencia del contrato, estos serán dotados de una cuenta para acceso al SAT – Sistema de Asistencia Técnica</li><li>Actualizaciones y mejoras para todos los productos comercializados por nuestra organización y que son de libre uso, es decir, que sean sin costo alguno o que estén amparados por contratos de mantenimiento del fabricante &nbsp;</li><li>Soporte remoto para todos los productos comercializados por nuestra organización. &nbsp;&nbsp;</li></ul><br>', '<p>Ofrecemos polizas de soporte y asistencia tecnica en las siguientes modalidades:</p><ul><li>Standard 8x5&nbsp;<br></li><li>Standard 12x7&nbsp;<br></li><li>Premium 12x7&nbsp;<br></li><li>Premium 24x7<br></li></ul><br>', '<br>', 3, 23, 1, 'soporte-post-venta-y-asistencia', '', 1, 0),
(7, 'DESARROLLO DE SOFTWARE', '<p><span style=\"text-align: justify;\">Contamos con un equipo de desarrollo especializado en aplicaciones de seguridad, web y de pago electrónico que posee las habilidades para crear software de calidad colocando a la seguridad siempre como prioridad.</span><br></p><p>Nuestro departamento de Desarrollo IT cuenta con experiencia en Servicios Web seguros, aplicaciones de autenticación robusta, criptografía avanzada, aplicaciones móviles blindadas y todo un conjunto de soluciones con las cuales puedes complementar tus aplicaciones actuales o crear toda una red de aplicaciones y telecomunicaciones segura para tu empresa. &nbsp; Toda empresa requiere seguridad a nivel de infraestructura y software utilizado y en nuestro departamento de Desarrollo IT encontrarás el aliado perfecto para conseguirlo. Además, ofrecemos soporte post producción y acompañamiento en todo el proceso de implementación de nuestros productos complementado con soporte 24/7 con especialistas dedicados a atender las solicitudes favoreciendo los tiempos de respuesta y calidad lograda.</p>', '<p><span style=\"text-align: justify;\">Todos nuestros desarrollos son realizados bajo las mejores practicas internacionales, y cada trabajo es gerenciado por un gerente de proyectos que asegura la calidad y tiempo acordado &nbsp;</span></p>', '<p><br></p><br>', 3, 18, 1, 'desarrollo-de-software', '', 1, 0),
(8, 'SERVICIOS DE CONSULTORÍA', '<p style=\"text-align: justify; \">Con la experiencia ganada a lo largo de nuestra trayectoria hemos logrado un elevado nivel de conocimientos en las áreas de seguridad, telecomunicaciones y servicios, la cual ponemos a disposición de nuestros clientes a través de nuestros Servicios de Consultoría</p><p style=\"text-align: justify; \">Contamos con profesionales debidamente entrenados, de amplia experiencia, en las áreas de arquitecturas de seguridad y comunicaciones, sistemas operativos, análisis de vulnerabilidades, análisis y administración de riesgo, monitoreo de seguridad, definición e implementación de políticas de seguridad informática y capacitación, todo ello con la finalidad de proteger los activos digitales de nuestros apreciados clientes. &nbsp;&nbsp;</p><p style=\"text-align: justify; \">Tenemos asociaciones estratégicas con las principales empresas internacionales fabricantes de software y hardware de seguridad e inteligencia informática, y nuestros ingenieros se encuentran constantemente explorando, evaluando y analizando nuevos productos, a fin de lograr asociaciones que le garanticen a nuestros clientes que tienen a su disposición productos y servicios de clase mundial.</p>', '<blockquote><p>Garantizamos total confidencialidad a nuestros clientes en cuanto al diseño, recomendaciones, e implantación de las soluciones de seguridad y comunicaciones.&nbsp;</p></blockquote><br>', '<p><br></p>', 3, 20, 1, 'servicios-de-consultoria', '', 1, 0),
(9, 'Desarrollo', 'Contamos con un área especializada en el desarrollo de soluciones de seguridad IT y financiera, en ella trabajamos a diario para ayudar a la banca a satisfacer las necesidades de sus clientes, acercándolos a ellos gracias a la tecnología. Nuestro portafolio incluye software de monitoreo de seguridad, generadores de tokens para smartphones y sistemas de pago P2P y P2B.', 'Contamos con un área especializada en el desarrollo de soluciones de seguridad IT y financiera, en ella trabajamos a diario para ayudar a la banca a satisfacer las necesidades de sus clientes, acercándolos a ellos gracias a la tecnología. Nuestro portafolio incluye software de monitoreo de seguridad, generadores de tokens para smartphones y sistemas de pago P2P y P2B.', 'Contamos con un área especializada en el desarrollo de soluciones de seguridad IT y financiera, en ella trabajamos a diario para ayudar a la banca a satisfacer las necesidades de sus clientes, acercándolos a ellos gracias a la tecnología. Nuestro portafolio incluye software de monitoreo de seguridad, generadores de tokens para smartphones y sistemas de pago P2P y P2B.', 2, 43, 1, 'desarrollo', '../administrador/site_media/images/archivos/folletos/folleto_Desarrollo.pdf', 1, 0),
(10, 'SAMF Pago Móvil P2P Interbancario', '<p class=\"MsoNormal\"><span lang=\"ES-VE\" style=\"font-size: 10.5pt; line-height: 107%; font-family: &quot;Open Sans&quot;; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Solución\nque</span>&nbsp;<span lang=\"ES\" style=\"font-size: 10.5pt; line-height: 107%; font-family: &quot;Open Sans&quot;; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">permite realizar transacciones financieras&nbsp;de pagos P2P Persona a\nPersona haciendo uso de teléfonos móviles inteligentes. Facilita pagos de\nproductos y servicios, y transferencia de dinero de persona a persona en\ncuestión de segundos, única aplicación del mercado que incorpora seguridad a través\nde claves dinámicas. Disponible para sistemas Android y IOS.</span><span lang=\"ES\"><o:p></o:p></span></p>', '<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><ul><li>Soporta autenticación con clava Estática y Dinámica (OTP)<br></li><li>Integrable a cualquier Switch Transaccional<br></li><li>Aplicación móvil nativa para IOS y Android<br></li><li>Cumple normas 641.10 de SUDEBAN<br></li><li>Pago rápido mediante código QR cifrado<br></li></ul></p>\n\n\n\n\n\n', '<p><span style=\"font-family: &quot;Open Sans&quot;;\">SAMF Pago Móvil P2P Interbancario se adapta a los requerimientos e imagen corporativa de la institución bancaria, cumple al 100% con la normativa 641.10 de SUDEBAN y a los requerimientos de ASOBANCA.</span></p>', 2, 43, 1, 'samf-pago-movil-p2p-interbancario', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_Pago Móvil P2P Interbancario.pdf', 1, 3),
(11, 'SAMF Token Móvil', '<p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 10.5pt; font-family: &quot;Open Sans&quot;;\">Generador de Tokens (OTPs) para dispositivos móviles\nAndroid y IOS que se integra al sistema de gestión de identidad SAMF para\nasegurar el ingreso a portales de Internet Bankng y para asegurar transacciones\nfinancieras mediante un proceso único de aseguramiento de transacciones\nmediante OTP compuestas que evita ataques de hombre en el medio\n(Man-in-the-middle) y hombre en el Browser (Man-in-the-Browser).<o:p></o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 10.5pt; font-family: &quot;Open Sans&quot;;\">Se adapta a la imagen corporativa de la institución\nbancaria.<o:p></o:p></span></p>', '<p style=\"margin-bottom: 0.0001pt; line-height: 17.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><ul><li>Seguridad\nsuperior a contraseña estática<br></li><li>Código de\nautenticación dinámico propio de la aplicación móvil vinculado a los\ncomponentes del dispositivo<br></li><li>Se vincula\nel usuario con un PIN de desbloqueo<br></li><li>Las\naplicaciones no pueden ser duplicadas en otro teléfono o dispositivo móvil.<br></li><li>Protocolo de\naprovisionamiento usando claves asimétricas de forma Online u Offline<br></li><li>Disponible para sistemas Android y IOS<br></li></ul></p>\n\n\n\n\n\n\n\n', '', 2, 18, 1, 'samf-token-movil', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_Token Móvil.pdf', 1, 2),
(12, 'SAMF P2P Mobile Payment', '<p>Solution that allows financial transactions of P2P payments Person to Person using smart mobile phones. It facilitates payments of products and services, and transfer of money from person to person in a matter of seconds, the only market application that incorporates security through dynamic keys. Available for Android and IOS systems.</p>', '<ul><li>Supports authentication with Static Password and Dynamic OTP</li><li>Integrable to any Transactional Switch&nbsp; </li><li>Native mobile application for IOS and Android &nbsp;&nbsp; </li><li>Fast payment by encrypted QR code</li></ul>', '<p>SAMF Mobile Payment P2P Interbancario adapts to the requirements and corporate image of the banking institution, meet strict 2FA security requirements.</p>', 5, 43, 1, 'samf-p2p-mobile-payment', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_P2P Mobile Payment.pdf', 2, 3),
(13, 'MANAGED SECURITY SERVICES', '<p>IT Security Solutions through its management of Professional Services puts at your disposal an advanced service of Managed Security with the purpose of Manage and Monitor in real time 7x24x365 its infrastructure of Telecommunications and Security.</p>', '<p>With this service, our clients guarantee that their infrastructure will be proactively monitored, allowing them to focus their efforts on their business scope. This service is specially designed for organizations that lack human resources with experience and / or monitoring and security capabilities, do not have enough personnel, or do not want to spend on security solutions, under this last premise, we offer the service of Managed Total Security Service, through which IT Security Solutions installs in the client&quot;s facilities the necessary systems which will have post-sale support policies for a monthly fee.</p>', '', 6, 23, 1, 'managed-security-services', '', 2, 4),
(14, 'SAMF - Multifactor Autentication Engine', '<p>It is an ultra-secure, next-generation identity management platform, geared towards the financial sector. Authentic and manages static and dynamic keys, has facilities for positive authentication, it is easy to integrate with any electronic channel of customer service of the bank in an SOA environment. &nbsp;&nbsp;</p><p> Applied correctly, SAMF strengthens the authentication through the different channels such as online banking, mobile banking, ATMs, P2P and P2B Mobile Payments, call center and other types of channels. SAMF is a multichannel authentication solution. This allows to implement advanced authentication mechanisms using multiple factors in a centralized manner. &nbsp;&nbsp; </p><p>It allows the creation and administration of the authentication policies of each channel through a single unified administration interface. It facilitates the administration of profiles and users of the system, it includes a powerful forensic tool with registration, storage and visualization of audit logs.</p>', '<ul><li>Centralized administration of the authentication process&nbsp; </li><li>Secure communication between SAMF and Channels&nbsp; </li><li>Easy integration with electronic customer service channels&nbsp; </li><li>Unlimited scalability&nbsp; </li><li>Rapid adaptation to changes in regulations and policies&nbsp; </li><li>Powerful audit and forensic tool</li></ul>', '', 5, 24, 1, 'samf---multifactor-autentication-engine', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_-_Multifactor_Autentication_Engine.pdf', 2, 1),
(15, 'SAMF Token Mobile', '<p>Token Generator (OTPs) for Android and IOS mobile devices that is integrated into the SAMF identity management system to ensure access to Internet Bankng portals and to ensure financial transactions through a single process of securing transactions through composite OTPs that avoid attacks of man in the middle (Man-in-the-middle) and man in the Browser (Man-in-the-Browser). </p><p>It adapts to the corporate image of the banking institution. &nbsp;</p>', '<ul><li>Security above static password</li><li>Dynamic authentication code of the mobile application linked to the components of the device</li><li>The user is linked with an unlock PIN </li><li>Applications can not be duplicated on another phone or mobile device</li><li>Provisioning protocol using asymmetric keys Online or Offline</li><li>Available for Android and IOS systems</li></ul>', '', 5, 18, 1, 'samf-token-mobile', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_Token Mobile.pdf', 2, 2),
(16, 'SATM - ATM Security Monitoring', '<p>It is a proactive security monitoring solution for ATMs networks, helps mitigate electronic fraud, monitors ATMs permanently and alerts to the presence of suspicious activities. </p><p>SATM consists of three basic elements: OSSIM, S-Server, S-Agent</p>', '<p>FUNCTIONALITIES&nbsp; </p><ul><li>Centralized Management </li><li>Real-Time Monitoring (HW and ATM SW) </li><li>Compilation of Audit Logs </li><li>Auto Protection of ATM Agent </li><li>Vulnerability Analysis </li><li>Forensic Tools </li><li>Compliance PCI Standards</li></ul>', '<p>KEY FEAUTURES</p><ul><li>Secure access via HTTPS </li><li>Administration of Users and Profiles</li><li>Logs of user activity</li><li>Vulnerability Analysis of ATM</li><li>Visor of Availability of ATM and Services </li><li>Configuration of Actions in the presence of suspicious activity </li><li>Viewer of Alarms, Traffic, Risk and Events </li><li>Forensic Reports Edition of Correlation Directives Dashboard of the system</li></ul>', 5, 25, 1, 'satm---atm-security-monitoring', '', 2, 4),
(17, 'CONSULTING SERVICES', '<p>In the experience gained throughout our career we have achieved a high level of knowledge in the areas of security, telecommunications and services, which we make available to our customers through our Consulting Services. </p><p>We have trained professionals with extensive experience in the areas of security and communications architectures, operating systems, vulnerability analysis, risk analysis and management, security monitoring, definition and implementation of IT security policies and training, all of which in order to protect the digital assets of our valued customers. </p><p>We have strategic partnerships with leading international software and hardware security and information technology companies, and our engineers are constantly exploring, evaluating and analyzing new products, in order to achieve partnerships that guarantee our customers that they have products at their disposal. and world class services.</p>', '<p>We guarantee total confidentiality to our clients regarding the design, recommendations, and implementation of security and communications solutions.</p>', '', 6, 20, 1, 'consulting-services', '', 2, 1),
(18, 'SOFTWARE DEVELOPMENT', '<p>We have a development team specialized in security, web and electronic payment applications that possess the skills to create quality software, always placing security as a priority. &nbsp;&nbsp; </p><p>Our IT Development group has experience in secure Web Services, robust authentication applications, advanced cryptography, shielded mobile applications and a whole set of solutions with which you can complement your current applications or create a secure network of applications and telecommunications for your company. &nbsp;&nbsp; </p><p>Every company requires security at the level of infrastructure and software used, in our IT Development department you will find the perfect partner to achieve it. In addition, we offer post production support and accompaniment throughout the implementation process of our products supplemented with 24/7 support with specialists dedicated to answering requests, favoring response times and achieved quality.</p>', '<p>All our developments are carried out under the best international practices, and each job is managed by a project manager who ensures the quality and time agreed.</p>', '', 6, 23, 1, 'software-development', '', 2, 2),
(19, 'TECHNICAL SUPPORT', '<p>Through our Professional Services Management, we offer support services and technical assistance to the software developed by our company, and third-party products marketed, including technical support for software, hardware, equipment maintenance and equipment replacement (RMA).</p><p><strong>Key Characteristics of the Technical Assistance Service</strong>: &nbsp;&nbsp; </p><ul><li>Unlimited support to incidents via phone, email and website&nbsp; </li><li>The Client can designate up to 2 people (support coordinators) who can contact our staff for support and assistance during the term of the contract they will be provided with an account for access to the SAT - Technical Assistance System&nbsp; </li><li>Updates and improvements for all products marketed by our organization and that are free to use, that is, free of charge or that are covered by manufacturer maintenance contracts&nbsp; </li><li>Remote support for all products marketed by our organization</li></ul>', '<p>We offer support policies and technical assistance in the following ways:&nbsp; </p><ul><li>Standard 8x5&nbsp; </li><li>Standard 12x7&nbsp; </li><li>Premium 12x7&nbsp; </li><li>Premium 24x7</li></ul>', '', 6, 23, 1, 'technical-support', '', 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones`
--

DROP TABLE IF EXISTS `direcciones`;
CREATE TABLE IF NOT EXISTS `direcciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `posicion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `direcciones`
--

INSERT INTO `direcciones` (`id`, `descripcion`, `estatus`, `id_idioma`, `titulo`, `posicion`) VALUES
(1, 'Av. Francisco de Miranda, Centro Empresarial Miranda, Piso 4, Oficina 4B, Urb. Los Ruices. \nCaracas - Venezuela\nCódigo Postal 1071', 1, 1, 'OFICINA VENEZUELA', 1),
(2, '6101	Blue Lagoon Dr, Suite 150,\n      Miami, FL, 33126', 1, 1, 'OFICINA EEUU', 2),
(3, 'Prueba', 0, 1, 'OFICINA SHANGAY', 0),
(4, 'Av. Francisco de Miranda, Centro Empresarial Miranda, Piso 4, Oficina 4B, Urb. Los Ruices. Caracas - Venezuela Código Postal 1071', 1, 2, 'VENEZUELA OFFICE', 2),
(5, '6101 Blue Lagoon Dr, Suite 150, Miami, FL, 33126', 1, 2, 'USA OFFICE', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion_mapas`
--

DROP TABLE IF EXISTS `direccion_mapas`;
CREATE TABLE IF NOT EXISTS `direccion_mapas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitud` text NOT NULL,
  `longitud` text NOT NULL,
  `id_direccion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `direccion_mapas`
--

INSERT INTO `direccion_mapas` (`id`, `latitud`, `longitud`, `id_direccion`) VALUES
(2, '25.961511', '-80.39116', 2),
(3, '10.4914821', '-66.8294758', 1),
(4, '53.397291', '-2.171772', 3),
(5, '10.4914821', '-66.8294758', 4),
(6, '25.961511', '-80.39116', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_nosotros`
--

DROP TABLE IF EXISTS `empresa_nosotros`;
CREATE TABLE IF NOT EXISTS `empresa_nosotros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `somos` text NOT NULL,
  `digital_agency` text NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa_nosotros`
--

INSERT INTO `empresa_nosotros` (`id`, `somos`, `digital_agency`, `id_imagen`, `id_idioma`, `estatus`) VALUES
(7, 'RF', 'HHHH', 105, 2, 1),
(8, 'III', 'IUIU', 105, 2, 1),
(9, 'FDFD', 'DFDF', 105, 2, 1),
(11, 'HHGH', 'GHGH', 105, 1, 1),
(14, 'BBB', 'HHH', 105, 1, 1),
(15, '<B><I><U>PRUEBA</U></I></B>', '<B><I>CON BOLD</I></B>', 105, 1, 1),
(16, 'PRUEBA', 'MOD', 105, 1, 1),
(17, '<B>PRUEBA</B>', '<I>PRUBEAAA</I>', 105, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus`
--

DROP TABLE IF EXISTS `estatus`;
CREATE TABLE IF NOT EXISTS `estatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estatus`
--

INSERT INTO `estatus` (`id`, `descripcion`) VALUES
(1, 'Activo'),
(2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `footer`
--

DROP TABLE IF EXISTS `footer`;
CREATE TABLE IF NOT EXISTS `footer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `correo` varchar(200) NOT NULL,
  `rif` varchar(30) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `footer`
--

INSERT INTO `footer` (`id`, `descripcion`, `correo`, `rif`, `id_idioma`, `estatus`) VALUES
(1, 'Nuestras soluciones de gestion de identidad y medios de pago disruptivos ultra seguros permiten a las instituciones financieras reducir el riesgo , asegurar las operaciones, optimizar el rendimiento de sus aplicaciones, llegar a nuevos clientes, mantener la fidelidad de los actuales, generar nuevos ingresos y posicionarse como lideres de la banca digital.', 'https://mail.itssca.net/', 'J-31119865-2', 1, 1),
(2, 'Our solutions of identity management and ultra-safe disruptive payment means allow financial institutions to reduce risk, ensure operations, optimize the performance of their applications, reach new customers, maintain the loyalty of current customers, generate new revenues and position themselves as leaders of digital banking.', 'https://mail.itssca.net/', '35-2509419', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria`
--

DROP TABLE IF EXISTS `galeria`;
CREATE TABLE IF NOT EXISTS `galeria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ruta` varchar(500) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galeria`
--

INSERT INTO `galeria` (`id`, `ruta`, `id_categoria`, `estatus`, `titulo`) VALUES
(96, 'assets/images/archivos/slider/laptop.jpeg', 1, 1, 'LAPTOP 2'),
(97, 'assets/images/archivos/marcas/dos.jpeg', 2, 1, 'dos'),
(98, 'assets/images/archivos/quienes_somos/imagen.jpeg', 3, 1, 'imagen'),
(99, 'assets/images/archivos/quienes_somos/imagen.jpeg', 3, 2, 'imagen'),
(100, 'assets/images/archivos/quienes_somos/dos.jpeg', 3, 1, 'dos'),
(101, 'assets/images/archivos/marcas/cofee.jpeg', 2, 2, 'cofee'),
(102, 'assets/images/archivos/noticias/noticias1.jpeg', 4, 1, 'noticias1'),
(103, 'assets/images/archivos/noticias/noticias2.jpeg', 4, 1, 'noticias2'),
(104, 'assets/images/archivos/noticias/noticias3.jpeg', 4, 1, 'noticias3'),
(105, 'assets/images/archivos/nosotros/Nosotros.jpeg', 20, 1, 'Nosotros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria_clientes`
--

DROP TABLE IF EXISTS `galeria_clientes`;
CREATE TABLE IF NOT EXISTS `galeria_clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_galeria` int(11) NOT NULL,
  `id_clientes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galeria_clientes`
--

INSERT INTO `galeria_clientes` (`id`, `id_galeria`, `id_clientes`) VALUES
(15, 70, 2),
(16, 70, 3),
(17, 71, 3),
(18, 72, 3),
(19, 73, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idioma`
--

DROP TABLE IF EXISTS `idioma`;
CREATE TABLE IF NOT EXISTS `idioma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `idioma`
--

INSERT INTO `idioma` (`id`, `descripcion`) VALUES
(1, 'Español'),
(2, 'Inglés');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `negocios`
--

DROP TABLE IF EXISTS `negocios`;
CREATE TABLE IF NOT EXISTS `negocios` (
  `id` int(11) DEFAULT NULL,
  `objetivo_negocio` text NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `palabras_claves`
--

DROP TABLE IF EXISTS `palabras_claves`;
CREATE TABLE IF NOT EXISTS `palabras_claves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `palabras_claves`
--

INSERT INTO `palabras_claves` (`id`, `descripcion`) VALUES
(66, 'pago movil'),
(67, ' qr'),
(68, ' pago con qr'),
(69, ' token movil'),
(70, ' P2P'),
(71, ' pago movil p2p'),
(72, ' pago movil sms'),
(73, ' pago movil p2p sms'),
(74, ' pago movil interbancario'),
(75, ' pago movil p2p interbancario'),
(76, ' p2c'),
(77, ' pago movil p2c'),
(78, '  pago movil p2c interbancario'),
(79, ' p2b'),
(80, ' pago p2b qr'),
(81, ' pago qr'),
(82, ' mobile payment'),
(83, ' qr payment'),
(84, ' p2p payment'),
(85, ' p2c payment'),
(86, ' p2b payment'),
(87, ' secure payment'),
(88, ' secure qr payment'),
(89, ' interbank payment'),
(90, ' p2p mobile payment'),
(91, ' p2c mobile payment'),
(92, ' p2b mobile payment'),
(93, ' secure mobile payment'),
(94, ' ultra secure mobile payment'),
(95, ' soft token'),
(96, ' soft token android'),
(97, ' soft token ios'),
(98, ' identity management'),
(99, ' identitity management financial'),
(100, ' media payment'),
(101, ' media payment secure solutions'),
(102, ' mobile media payment');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

DROP TABLE IF EXISTS `personas`;
CREATE TABLE IF NOT EXISTS `personas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` int(11) NOT NULL,
  `nombres_apellidos` varchar(100) NOT NULL,
  `estatus` int(11) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `cedula`, `nombres_apellidos`, `estatus`, `telefono`, `email`) VALUES
(1, 9999999, 'Administrador', 1, '000000000', 'content.manager@gmail.com'),
(3, 18610972, 'Grecia Quintero', 0, '0000000000', 'grecia.quintero@itssca.com'),
(4, 23707617, 'Gustavo Lara', 1, '04122484877', 'gustavo.lara@itssca.net'),
(5, 5533039, 'Miguel Porro', 1, '+58-000-000-00-00', 'miguel.porro@itssca.net'),
(8, 18042153, 'Gianni Santucci', 0, '+5800000000000000', 'gsantucci@loquesea.com'),
(9, 1744444444, 'gustavo ocanto', 0, '+58-416-555-55-66', 'asdf@gmail.com'),
(10, 888899999, 'prueba de usuario', 0, '+5899999999999999', 'prueba@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portafolio`
--

DROP TABLE IF EXISTS `portafolio`;
CREATE TABLE IF NOT EXISTS `portafolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_idioma` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `url` varchar(155) NOT NULL,
  `cliente` varchar(255) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `slug` text NOT NULL,
  `posicion` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `portafolio`
--

INSERT INTO `portafolio` (`id`, `id_idioma`, `id_servicio`, `titulo`, `descripcion`, `fecha_creacion`, `url`, `cliente`, `id_imagen`, `estatus`, `slug`, `posicion`) VALUES
(1, 1, 11, 'Página web euroglobalrsi', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-17', 'www.euroglobalrsi.com', 'euroglobalrsi', 74, 1, 'pgina-web-euroglobalrsi', 2),
(2, 1, 11, 'Aktive', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-17', '', 'aktive', 66, 1, 'aktive', 1),
(3, 1, 11, 'Esaica', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-25', 'www.esaica.esy.es', 'esaica', 77, 1, 'esaica', 3),
(4, 2, 12, 'Euroglobalrsi', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-25', 'www.euroglobalrsi.com', 'euroglobalrsi', 74, 1, 'euroglobalrsi', 2),
(5, 2, 12, 'Aktive', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-25', 'www.aktive.com', 'Aktive', 66, 1, 'aktive', 1),
(6, 1, 5, 'Marca empresa', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-25', '', 'Etc', 68, 1, 'marca-empresa', 1),
(7, 2, 12, 'Esaica', '<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus diam arcu, ullamcorper sit', '2018-11-25', 'www.esaica.esy.es', 'Esiaca', 77, 1, 'esaica', 3),
(8, 1, 6, 'Logos', '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam in blandit libero. Proin vitae egestas risus. Nulla consequat', '2018-11-30', '', 'Mcdonalds', 76, 1, 'logos', 0),
(9, 2, 9, 'Logos', '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam in blandit libero. Proin vitae egestas risus. Nulla consequat', '2018-11-28', '', 'Mcdonalds', 76, 1, 'logos', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redes`
--

DROP TABLE IF EXISTS `redes`;
CREATE TABLE IF NOT EXISTS `redes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enlace` varchar(200) NOT NULL,
  `id_tipo_redes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `red_social`
--

DROP TABLE IF EXISTS `red_social`;
CREATE TABLE IF NOT EXISTS `red_social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_red` varchar(100) NOT NULL,
  `id_tipo_red` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `red_social`
--

INSERT INTO `red_social` (`id`, `url_red`, `id_tipo_red`) VALUES
(5, 'https://www.instagram.com/p/BYn2otbFapp/', 3),
(6, 'https://www.facebook.com/IT-Security-Solutions-182264735648689/', 1),
(10, 'https://www.linkedin.com/company/1057055/', 4),
(11, 'https://twitter.com/ITSecurity_ve', 2),
(12, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion_galeria`
--

DROP TABLE IF EXISTS `seccion_galeria`;
CREATE TABLE IF NOT EXISTS `seccion_galeria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_imagen` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion_noticias`
--

DROP TABLE IF EXISTS `seccion_noticias`;
CREATE TABLE IF NOT EXISTS `seccion_noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(300) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `slug` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `seccion_noticias`
--

INSERT INTO `seccion_noticias` (`id`, `titulo`, `id_imagen`, `descripcion`, `estatus`, `fecha`, `id_usuario`, `slug`, `id_idioma`) VALUES
(9, 'NOTICIA PRINCIPAL', 102, '<I><U><B>BBBBB</B></U></I>', 1, '2019-01-08', 1, 'noticia-principal', 1),
(10, 'NOTICIA DOS', 104, 'NOTICIA DOS', 1, '2019-01-06', 1, 'noticia-dos', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

DROP TABLE IF EXISTS `servicios`;
CREATE TABLE IF NOT EXISTS `servicios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_imagen` int(11) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id`, `id_imagen`, `titulo`, `descripcion`, `estatus`, `id_idioma`, `posicion`) VALUES
(1, 56, 'Primera', 'Primera Prueba', 2, 1, 0),
(2, 55, 'Prueba', '2', 2, 1, 0),
(3, 57, 'Prueba Mod', 'sdsds', 2, 1, 0),
(4, 57, 'prueba 4', 'prueba hoy', 2, 1, 0),
(5, 61, 'MARKETING DIGITAL', '<h3><sub>Nuestras estrategias permiten alcanzar los objetivos de negocio e impulsar las marcas.</sub></h3><h3><sub><br></sub><ul><li><sub>Estrategia de Marketing Digital</sub></li></ul><ul><li><sub>Marketing de Contenidos</sub></li></ul><ul><li><sub>Estrategia de Redes Sociales</sub></li></ul><ul><li><sub>Posicionamiento SEO &amp; SEM</sub></li></ul><ul><li><sub>E-Mail Marketing</sub></li></ul><ul><li><sub>Publicidad Display</sub></li></ul></h3>', 1, 1, 6),
(6, 60, 'DISEÑO GRÁFICO', '<h3><sub>Ofrecemos soluciones innovadoras e impactantes para una comunicación visual efectiva de su empresa.<br></sub><h3><sub>Nuestro servicio incluye:</sub></h3><h3><br><ul><li><sub>Diseño de Logotipos</sub></li></ul><ul><li><sub>Identidad Corporativa</sub></li></ul><ul><li><sub>Creación de Marca</sub></li></ul><ul><li><sub>Diseño Editorial</sub></li></ul><ul><li><sub>Folletos y Catálogos</sub></li></ul><ul><li><sub>Diseño de Empaques</sub></li></ul></h3></h3>', 1, 1, 1),
(7, 63, 'REDES SOCIALES', '<h3 style=\"text-align: justify;\"><sub>GESTIÓN DE REDES SOCIALES:<br></sub></h3><h3 style=\"text-align: justify;\"><sub>Construimos y gestionamos comunidades alrededor de los negocios en las plataformas de Redes Sociales.&nbsp;<br>Nuestro servicio ayuda a su empresa a mejorar la presencia en línea, fortalecer la relación con sus clientes, aumentar el tráfico web y obtener reconocimiento de la marca.</sub></h3><h3 style=\"text-align: justify;\"><sub><br></sub><ul><li><sub>Creación de Perfiles Sociales</sub></li></ul><ul><li><sub>Gestión Estratégica de Redes Sociales</sub></li></ul><ul><li><sub>Dinamización de Comunidades Online&nbsp;</sub></li></ul><ul><li><sub>Creación de Contenido en las Plataformas Sociales</sub></li></ul><ul><li><sub>Informe de Gestión</sub></li></ul></h3>', 1, 1, 3),
(8, 63, 'SOCIAL MEDIA', '<h3><sub>SOCIAL MEDIA MANAGEMENT:<br></sub></h3><h3><sub>We build and manage communities around business in the different Social Media platforms.&nbsp;<br></sub></h3><h3></h3><h3><sub>Our service help your company to enhance online presence, strengthen relationship with your clients, increase web traffic and gain brand awareness.</sub></h3><span style=\"font-size: 18px; line-height: 0;\"><sub><br></sub></span><h3><ul><li><sub><span style=\"font-size: 18px; line-height: 0;\">Social Profiles Creation</span></sub></li></ul><ul><li><sub><span style=\"font-size: 18px; line-height: 0;\">Strategic Social Media Management</span></sub></li></ul><ul><li><sub><span style=\"font-size: 18px; line-height: 0;\">Dynamizing Online Communities</span></sub></li></ul><ul><li><sub><span style=\"font-size: 18px; line-height: 0;\">Content Creation on Social platforms</span></sub></li></ul></h3><h3><ul><li><sub>Social Media Management Reporting</sub></li></ul></h3>', 1, 2, 2),
(9, 60, 'GRAPHIC DESIGN', '<h3><sub>We offer innovative and impactful solutions for an effective visual communication of your company.<br></sub><h3><sub>Our service includes:</sub><h3><h3><h3><ul><li><sub>Logo Design</sub></li></ul><ul><li><sub>Corporate Identity</sub></li></ul><ul><li><sub>Branding</sub></li></ul><ul><li><sub>Editorial Design</sub></li></ul><ul><li><sub>Brochures and catalogs</sub></li></ul><ul><li><sub>Packaging Design</sub></li></ul></h3></h3></h3></h3></h3><p><sub></sub></p>', 1, 2, 1),
(10, 61, 'DIGITAL MARKETING', '<h3><sub>Our strategies allow to achieve the business objectives and boost the brands</sub></h3><h3><sub><br></sub><ul><li><sub>Digital Marketing Strategy</sub></li></ul><ul><li><sub>Content Marketing</sub></li></ul><ul><li><sub>Social Media Strategy</sub></li></ul><ul><li><sub>SEO &amp; SEM</sub></li></ul><ul><li><sub>E-Mail Marketing</sub></li></ul><ul><li><sub>Display Advertising</sub></li></ul></h3>', 1, 2, 3),
(11, 62, 'DESARROLLO WEB', '<h3></h3><h3><sub>DISEÑO Y DESARROLLO WEB:<br></sub></h3><h3><sub>Creamos sitios web profesionales, funcionales y optimizados para múltiples dispositivos.</sub></h3><h3><sub><br></sub></h3><h3 style=\"text-align: justify;\"></h3><h3><ul><li><sub>Sitios Web Corporativos</sub></li></ul><ul><li><sub>Diseño Web Responsive</sub></li></ul><ul><li><sub>Soluciones Ecommerce</sub></li></ul><ul><li><sub>Sitios Web Personalizados</sub></li></ul><ul><li><sub>Blogs y Landing Pages</sub></li></ul><ul><li><sub>Mantenimiento de Sitios Web</sub></li></ul></h3>', 1, 1, 2),
(12, 62, 'WEB DESIGN', '<h3 style=\"text-align: justify;\"><sub>WEB DESIGN &amp; DEVELOPMENT:<br></sub></h3><h3 style=\"text-align: justify;\"><sub>We build and manage communities around business in the different Social Media platforms.&nbsp;<br></sub><sub>Our service help your company to enhance online presence, strengthen relationship with your clients, increase web traffic and gain brand awareness.</sub></h3><h3 style=\"text-align: justify;\"><br><ul><li><sub>Social Profiles Creation</sub></li></ul><ul><li><sub>Strategic Social Media Management</sub></li></ul><ul><li><sub>Dynamizing Online Communities</sub></li></ul><ul><li><sub>Content Creation on Social platforms</sub></li></ul><ul><li><sub>Social Media Management Reporting</sub></li></ul></h3>', 1, 2, 4),
(13, 59, 'SEO', '<h3><sub>SEARCH ENGINE OPTIMIZATION (SEO):<br></sub></h3><h3><sub>We increase the visibility of your business website in the most popular search engines in order to generate quality organic traffic, improve brand image and attract more potential customers.</sub></h3><h3><sub><br>Our SEO service includes:</sub></h3><h3><sub><br></sub><ul><li><sub>Web Technical Audit</sub></li></ul><ul><li><sub>Keyword Research</sub></li></ul><ul><li><sub>Content Strategy&nbsp;</sub></li></ul><ul><li><sub>On/Off-Page Optimization</sub></li></ul><ul><li><sub>Report and Monitoring</sub></li></ul></h3>', 1, 2, 5),
(14, 59, 'SEO', '<h3><sub>OPTIMIZACIÓN DE MOTORES DE BÚSQUEDA (SEO):<span style=\"font-size: 18px; line-height: 0px;\">&nbsp;</span></sub></h3><h3><sub>Aumentamos la visibilidad del sitio web de su negocio en los motores de búsqueda más populares con el objeto de generar tráfico orgánico de calidad, mejorar la imagen de marca y atraer más clientes potenciales.</sub></h3><h3><sub><br></sub></h3><h3><sub>Nuestro servicio SEO incluye:</sub></h3><p><sub><br></sub></p><h3><ul><li><sub>Auditoria Técnica Web<br></sub></li><li><sub>Investigación de Palabras Claves<br></sub></li><li><sub>Estrategia de Contenido&nbsp;<br></sub></li><li><sub>Optimización On/Off del Sitio Web<br></sub></li><li><sub>Reporte y Seguimiento</sub><br></li></ul></h3>', 1, 1, 4),
(15, 64, 'CONSULTING', '<h3 style=\"text-align: justify;\"><sub>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sem eros, ullamcorper vel convallis a, convallis a quam. Nullam porta mauris augue, a imperdiet elit tempus vitae. Quisque venenatis nunc id nisl varius hendrerit. Nullam sit amet auctor m</sub></h3>', 1, 2, 6),
(16, 64, 'CONSULTORÍA', '<h3 style=\"text-align: justify;\"><sub>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sem eros, ullamcorper vel convallis a, convallis a quam. Nullam porta mauris augue, a imperdiet elit tempus vitae. Quisque venenatis nunc id nisl varius hendrerit. Nullam sit amet auctor m</sub></h3>', 1, 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_texto`
--

DROP TABLE IF EXISTS `servicio_texto`;
CREATE TABLE IF NOT EXISTS `servicio_texto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_text` varchar(150) NOT NULL,
  `texto` varchar(250) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicio_texto`
--

INSERT INTO `servicio_texto` (`id`, `titulo_text`, `texto`, `estatus`, `id_idioma`, `posicion`) VALUES
(8, 'roxi', 'prueba', 2, 1, 1),
(9, 'dsdsds', 'sdsdsd', 0, 2, 1),
(10, 'CUÁL ES NUESTRO TRABAJO', '<p><span style=\"color: rgb(119, 119, 119); font-family: Roboto-Light; font-size: 20px; line-height: 35px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sem eros, ullamcorper vel convallis a, convallis a quam. Nul', 1, 1, 1),
(11, 'WHAT WE DO', '<h3><span style=\"text-align: justify;\"><sub>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sem eros, ullamcorper vel convallis a, convallis a quam. Nullam porta mauris augue, a imperdiet elit tempus vitae. Quisque venenatis nunc id nis', 1, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text NOT NULL,
  `descripcion` text NOT NULL,
  `imagen` varchar(300) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `boton_slider` varchar(200) NOT NULL,
  `url_slider` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `slider`
--

INSERT INTO `slider` (`id`, `titulo`, `descripcion`, `imagen`, `id_idioma`, `boton_slider`, `url_slider`) VALUES
(254, 'DIGITAL MARKETING', '\n				<p><span style=color: rgb0,= 0,= 0;= font-family:= &quot;open= sans&quot;,= arial,= sans-serif;= line-height:= 20px;= text-align:= justify;=>Lorem ipsum dolor sit amet consectetur adipiscing elit. Suspendisse</span></p>\n				', '../administrador/site_media/images/archivos/slider/slider_1.jpeg', 2, 'VIEW PORTFOLIO', 'portafolio'),
(255, 'GRAPHIC DESIGN', '\n				<p><span style=color: rgb0,= 0,= 0;= font-family:= &quot;open= sans&quot;,= arial,= sans-serif;= line-height:= 20px;= text-align:= justify;=>Lorem ipsum dolor sit amet consectetur adipiscing elit. Suspendisse</span></p>\n				', '../administrador/site_media/images/archivos/slider/slider_2.jpeg', 2, 'CONTACT', 'contact'),
(256, 'WEB DEVELOPMENT', '<p><span style=color: rgb0, 0, 0; font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px; text-align: justify;>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse</span></p>', '../administrador/site_media/images/archivos/slider/slider_3.jpeg', 2, 'PORTFOLIO', 'portfolio'),
(257, 'MARKETING DIGITAL', '\n								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam venenatis, orci sed hendrerit interdum.</p>				\n				', '../administrador/site_media/images/archivos/slider/slider_1.jpeg', 1, 'VER PORTAFOLIO', 'PORTAFOLIO'),
(258, 'DISEÑO GRÁFICO', '\n				\n				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam venenatis, orci sed hendrerit interdum.</p>\n				\n				', '../administrador/site_media/images/archivos/slider/slider_2.jpeg', 1, 'VER PORTAFOLIO', 'portafolio'),
(259, 'DESARROLLO WEB', '\n				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam venenatis, orci sed hendrerit interdum.</p>\n				', '../administrador/site_media/images/archivos/slider/slider_3.jpeg', 1, 'CONTÁCTANOS', 'contactanos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefonos`
--

DROP TABLE IF EXISTS `telefonos`;
CREATE TABLE IF NOT EXISTS `telefonos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_direccion` int(11) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefonos`
--

INSERT INTO `telefonos` (`id`, `id_direccion`, `telefono`) VALUES
(15, 1, '+58-212-234-24-44'),
(16, 1, '+58-412-219-48-77'),
(23, 3, '+5609998877'),
(26, 4, '+58-212-234-24-44'),
(27, 4, '+58-412-219-48-77'),
(32, 5, '+1-786-288-01-75'),
(33, 5, '+1-305-262-28-15'),
(34, 2, '+1-786-288-01-75'),
(35, 2, '+1-305-262-28-15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_usuarios`
--

DROP TABLE IF EXISTS `tipos_usuarios`;
CREATE TABLE IF NOT EXISTS `tipos_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipos_usuarios`
--

INSERT INTO `tipos_usuarios` (`id`, `descripcion`) VALUES
(1, 'administrador'),
(2, 'usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_negocio`
--

DROP TABLE IF EXISTS `tipo_negocio`;
CREATE TABLE IF NOT EXISTS `tipo_negocio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_negocio`
--

INSERT INTO `tipo_negocio` (`id`, `titulo`, `descripcion`, `estatus`, `id_idioma`, `posicion`) VALUES
(1, 'Integración de Soluciones', '<p style=\"margin-bottom: 0.0001pt; line-height: 17.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size:10.5pt;font-family:&quot;Open Sans&quot;;\nmso-bidi-font-family:Helvetica;color:#333333;mso-ansi-language:ES\" lang=\"ES\">Contamos con\namplia experiencia en el área de integraci</span><span style=\"font-size:11.0pt;font-family:&quot;Calibri&quot;,sans-serif;color:black;\nmso-ansi-language:ES\" lang=\"ES\">ó</span><span style=\"font-size:10.5pt;font-family:\n&quot;Open Sans&quot;;mso-bidi-font-family:Helvetica;color:#333333;mso-ansi-language:\nES\" lang=\"ES\">n de soluciones de seguridad y telecomunicaciones, a la fecha hemos\nrealizado innumerables implementaciones en los sectores de Finanzas, Gobierno,\nTelecomunicaciones, Seguros, Industria y Comercio en general.<o:p></o:p></span></p>\n\n<span style=\"font-size:10.5pt;font-family:&quot;Open Sans&quot;;\nmso-bidi-font-family:Helvetica;color:#333333;mso-ansi-language:ES\" lang=\"ES\">&nbsp;</span>\n\n<p style=\"margin-bottom: 0.0001pt; line-height: 17.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size:10.5pt;font-family:&quot;Open Sans&quot;;\nmso-bidi-font-family:Helvetica;color:#333333;mso-ansi-language:ES\" lang=\"ES\">Nuestra\nfortaleza se basa en un equipo humano integrado por ingenieros debidamente\nentrenados y certificados en las marcas que comercializamos, que tienen como misión\nprestar servicios de preventa y postventa de <strong>excelencia.</strong><o:p></o:p></span></p>', 1, 1, 3),
(2, 'Portafolio de Soluciones', '<p>Contamos con un área especializada en el desarrollo de soluciones de seguridad IT y financiera, en ella trabajamos a diario para ayudar a la banca a satisfacer las necesidades de sus clientes, acercándolos a ellos gracias a la tecnología. Nuestro portafolio incluye software de gestión de identidad, generadores de tokens para smartphones y sistemas de pago P2P, P2C y P2B ultra seguros.</p>', 1, 1, 1),
(3, 'Servicios Profesionales', '<p>Ponemos a disposición de nuestros clientes una amplia gama de servicios de soporte técnico, entrenamiento, consultoría, control de proyectos y pólizas de soporte en variadas modalidades que se ajustan según sus necesidades.</p>', 1, 1, 2),
(4, 'Ejemplo de modificación', ' Lorem ipsum dolor', 0, 1, 0),
(5, 'Solutions Portfolio', '<p>We have an area specialized in the development of IT and financial security solutions, in which we work daily to help banks meet the needs of their customers, bringing them closer to them thanks to technology. Our portfolio includes identity management software, token generators for smartphones and P2P, P2C and P2B ultra secure payment systems.</p>', 1, 2, 0),
(6, 'Professional Services', '<p>We offer our clients a wide range of technical support services, training, consulting, project control and support policies in various ways that are adjusted according to your needs.</p>', 1, 2, 0),
(7, 'Integration Services', '<p>We have extensive experience in the area of integration of security and telecommunications solutions, to date we have made countless implementations in the sectors of Finance, Government, Telecommunications, Insurance, Industry and Commerce in general. &nbsp; &nbsp;&nbsp; Our strength is based on a human team made up of engineers duly trained and certified in the brands we market, whose mission is to provide pre-sales and after-sales services of excellence. &nbsp; &nbsp; &nbsp; &nbsp;</p>', 1, 2, 0),
(8, 'prueba de registro cargar  modificar', 'Esto es una prueba de registro de modificación, el registro ha sido modificado<br>', 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_red`
--

DROP TABLE IF EXISTS `tipo_red`;
CREATE TABLE IF NOT EXISTS `tipo_red` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_red`
--

INSERT INTO `tipo_red` (`id`, `descripcion`) VALUES
(1, 'Facebook'),
(2, 'Twitter'),
(3, 'Instagram'),
(4, 'Linkedin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_redes`
--

DROP TABLE IF EXISTS `tipo_redes`;
CREATE TABLE IF NOT EXISTS `tipo_redes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_redes`
--

INSERT INTO `tipo_redes` (`id`, `descripcion`) VALUES
(1, 'Facebook'),
(2, 'Twitter');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` int(11) NOT NULL,
  `id_tipo_usuario` int(11) NOT NULL,
  `login` varchar(150) NOT NULL,
  `clave` varchar(150) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `id_persona`, `id_tipo_usuario`, `login`, `clave`, `estatus`) VALUES
(1, 1, 1, 'administrador', '54fcaf4b9a819766aff69755f649d5ea3fbe8468', 1),
(3, 3, 1, 'gquintero', '7c222fb2927d828af22f592134e8932480637c0d', 0),
(4, 4, 1, 'glara', '7c222fb2927d828af22f592134e8932480637c0d', 1),
(5, 5, 1, 'mporro', '7c222fb2927d828af22f592134e8932480637c0d', 1),
(8, 8, 1, 'gsantucci', '7c222fb2927d828af22f592134e8932480637c0d', 1),
(9, 9, 1, 'gocanto', '7c222fb2927d828af22f592134e8932480637c0d', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

DROP TABLE IF EXISTS `videos`;
CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text NOT NULL,
  `link` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id`, `titulo`, `link`, `estatus`, `id_idioma`) VALUES
(1, 'SAMF Pago movil, noticias Globovisión tecnología', 'https://www.youtube.com/watch?v=8QmIyF7q3vI', 1, 1),
(2, 'Pago P2P Noticias globovisión tecnología', 'https://www.youtube.com/watch?v=NcS4g3lN_eU&t=53s', 0, 1),
(3, 'BanescoPagoMovil', 'https://youtu.be/jBFZXhpvYKo', 0, 1),
(4, 'Pago Móvil Banesco', 'https://www.youtube.com/watch?v=jBFZXhpvYKo', 1, 1),
(5, 'Entrevista a  Deiniel Cardenas Banesco Pago Móvil', 'https://www.youtube.com/watch?v=z33RIoHKKBY&feature=youtu.be', 1, 1),
(6, 'BANESCO PAGO MOVIL', 'https://youtu.be/JVUT2YBJTw0', 0, 1),
(7, 'BANESCO PAGO MÓVIL', 'https://www.youtube.com/watch?v=JVUT2YBJTw0&feature=youtu.be', 1, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
