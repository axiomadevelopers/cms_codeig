angular.module("ContentManagerApp")
//---------------------------------------------------------------------------------------
//--Bloque de servicios
//--Servicio para cargar imagenes...
.service('upload', ["$http", "$q", function ($http, $q)
{
	this.uploadFile = function(file, categoria,nombre_archivo,base_url)
	{
		var deferred = $q.defer();
		var formData = new FormData();
		formData.append("categoria", categoria);
		formData.append("file", file);
		formData.append("nombre_archivo",nombre_archivo);
		return $http.post(base_url+"GaleriaMultimedia/upload", formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	},
	this.uploadFileFolleto = function(file, id_negocio,nombre_archivo)
	{
		var deferred = $q.defer();
		var formData = new FormData();

		formData.append("id_negocio", id_negocio);
		formData.append("file", file);
		/*formData.append("categoria", categoria);
		formData.append("file", file);*/
		formData.append("nombre_archivo",nombre_archivo);
		return $http.post("./controladores/archivosController.php", formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	},
	this.uploadFileBlog = function(file, id_blog,nombre_archivo)
	{
		var deferred = $q.defer();
		var formData = new FormData();

		formData.append("id_blog", id_blog);
		formData.append("file", file);
		/*formData.append("categoria", categoria);
		formData.append("file", file);*/
		formData.append("nombre_archivo",nombre_archivo);
		return $http.post("./controladores/archivosBlogController.php", formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	}
}])
.factory("categoriasFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_categoria = id;
				else
					id_categoria = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_categorias : function(callback){
				$http.post(base_url+"/Categorias/consultarCategoriasTodas", { id_idioma:id_idioma,id_categoria:id_categoria}).success(callback);
			}
	}
}])

.factory("nosotrosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_nosotros = id;
				else
					id_nosotros = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_nosotros : function(callback){
				$http.post(base_url+"/Nosotros/consultarNosotrosTodos", { id_idioma:id_idioma,id_nosotros:id_nosotros}).success(callback);
			}
	}
}])
//--Servicio para consulta de noticias
.factory("noticiasFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_noticias = id;
				else
					id_noticias = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_noticias : function(callback){
				$http.post(base_url+"/Noticias/consultarNoticiasTodas", { id_idioma:id_idioma,id_noticias:id_noticias}).success(callback);
			}
	}
}])
//--Servicio para compartir datos de mensajes iniciales
.service('serverDataMensajes',[function(){
	var puente = [];
	this.puenteData = function(arreglo){
		puente = arreglo;
		return puente;
	}
	this.obtener_arreglo = function(){
		return puente;
	}
	this.limpiar_arreglo_servicio = function (){
		puente = [];
		return puente;
	}
}])
//--Bloque de factorias
//Factory para verificar inicio de sesion
.factory("sesionFactory",['$http', function($http){
	return{
			datos_sesion : function(callback){
				$http.post("./controladores/fbasicController.php", { accion:'datos_sesion'}).success(callback);
			},
			sesion_usuario : function(callback){
				$http.post("./controladores/fbasicController.php", { accion:'consultar_sesion'}).success(callback);
			},
			cerrar_sesion: function(callback){
				$http.post("./controladores/fbasicController.php", { accion:'cerrar_sesion'}).success(callback);
			}
	}
}])

//Para consultar idiomas
/*.factory("idiomaFactory",['$http', function($http){
	return{
			cargar_idioma : function(callback){
				$http.post("./controladores/quienesSomosController.php", { accion:'consultar_idioma'}).success(callback);
			}
	}
}])*/

.factory("idiomaFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(base_url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_idioma : function(callback){
				$http.post(base_url+"Nosotros/consultar_idioma", { id_idioma:id_idioma,}).success(callback);
			}
	}
}])
//Para la galeria
.factory("galeriaFactory",['$http', function($http){

	return{
			asignar_valores : function (id,nombre,url){
				if(id!="")
					categoria = id;
				else
					categoria = "";

				if(nombre!=""){
					titulo_imagen = nombre;
				}
				else{
					titulo_imagen = "";
				}
				if(url !=""){
					base_url = url;
				}else{
					base_url = "";
				}
			},
			cargar_galeria_fa : function(callback){
				$http.post(base_url+"/GaleriaMultimedia/consultarGaleriaCategoria",{ categoria:categoria, nombre:titulo_imagen}).success(callback);
			},
	}
}])
//Para las citas
.factory("citasProgramadasFactory",['$http', function($http){

	return{
			valor_estatus : function (estatus_op, especialidad, doctor){
				if(estatus_op!="")
					estatus = estatus_op;
				else
					estatus = "";
				if(especialidad!="")
					id_especialidad = especialidad;
				else
					id_especialidad = "";
				if(doctor!="")
					id_doctor = doctor
				else
					id_doctor = doctor
			},
			cargar_citas_programadas : function(callback){
				$http.post("./controladores/citasController.php",{ accion:'consultar_citas_programadas', estatus:estatus, id_especialidad:id_especialidad, id_doctor:id_doctor }).success(callback);
			}
	}
}])
.factory("paisesFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_paises : function(callback){
			$http.post("./controladores/paisesController.php", {accion:'consultar_paises',id_idioma:id_idioma}).success(callback);
		}
	}
}])
.factory("estadosFactory",['$http', function($http){
	return{
		asignar_valores : function (pais){
			if(pais!="")
				id_pais = pais;
			else
				id_pais = "";
		},
		cargar_estados : function(callback){
			$http.post("./controladores/paisesController.php", {accion:'consultar_estados',id_pais:id_pais}).success(callback);
		}
	}
}])
.factory("ciudadFactory",['$http', function($http){
	return{
		asignar_valores : function (estado){
			if(estado!="")
				id_estado = estado;
			else
				id_estado = "";
		},
		cargar_ciudades : function(callback){
			$http.post("./controladores/paisesController.php", {accion:'consultar_ciudades',id_estado:id_estado}).success(callback);
		}
	}
}])
/*.factory("tiposNegociosFactory",['$http', function($http){
	return{
		valor_estatus : function (estatus_op){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
		},
		cargar_tiposNegocios : function(callback){
			$http.post("./controladores/tiposNegociosController.php", {accion:'consultar_tipos_negocios'}).success(callback);
		}
	}
}])*/
.factory("tiposServiciosFactory",['$http', function($http){
	return{
		asignar_valor : function (estatus_op,idioma){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_servicio : function(callback){
			$http.post("./controladores/serviciosprincipalController.php", {accion:'consultar_servicios',id_idioma:id_idioma,estatus:estatus}).success(callback);
		}
	}
}])
////
.factory("tiposNegociosFactory",['$http', function($http){
	return{
		asignar_valor : function (estatus_op,idioma){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_tiposNegocios : function(callback){
			$http.post("./controladores/tiposNegociosController.php", {accion:'consultar_tipos_negocios',id_idioma:id_idioma,estatus:estatus}).success(callback);
		}
	}
}])

.factory("serviciosFactory",['$http', function($http){
	return{
		asignar_valor : function (estatus_op,idioma){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
			if(idioma!="")
				idioma = idioma;
			else
				idioma = "";
		},
		cargar_servicios : function(callback){
			$http.post("./controladores/detallesPortafolioController.php", {accion:'consultar_servicios',idioma:idioma,estatus:estatus}).success(callback);
		}
	}
}])
.factory("tiposNegociosOrdenFactory",['$http', function($http){
	return{
		asignar_valor : function (estatus_op,idioma){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_tiposNegocios : function(callback){
			$http.post("./controladores/tiposNegociosController.php", {accion:'consultar_tipos_negocios_orden',id_idioma:id_idioma,estatus:estatus}).success(callback);
		}
	}
}])
.factory("negociosOrdenFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma,tipos_negocios){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";

			if(tipos_negocios!="")
				id_tipos_negocios = tipos_negocios;
			else
				id_tipos_negocios = "";
		},
		cargar_detallesNegocios : function(callback){
			$http.post("./controladores/detallesNegociosController.php", {accion:'consultar_tipos_negocios_orden','id_idioma':id_idioma,'tipo_negocio':id_tipos_negocios}).success(callback);
		}
	}
}])
.factory("tiposUsuariosFactory",['$http', function($http){
	return{
		cargar_usuarios : function(callback){
			$http.post("./controladores/usuariosController.php", {accion:'consultar_tipos_usuarios'}).success(callback);
		}
	}
}])
.factory("contactosFactory",['$http', function($http){
	return{
		cargar_contactos : function(callback){
			$http.post("./controladores/contactosController.php", {accion:'consultar_clientes'}).success(callback);
		},
		cargar_contactos_empleos : function(callback){
			$http.post("./controladores/contactosController.php", {accion:'consultar_empleos'}).success(callback);
		}
	}
}])
.factory("redesFactory",['$http', function($http){
	return{
		asignar_tipo_red : function (valor_id_tipo_red){
			if(valor_id_tipo_red!=""){
				id_tipo_red= valor_id_tipo_red;
			}else{
				id_tipo_red = ""
			}
		},
		cargar_redes_detalles: function(callback){
			$http.post("./controladores/redesController.php", {accion:'consultar_redes_detalle', 'id_tipo_red':id_tipo_red}).success(callback);
		},
		cargar_redes : function(callback){
			$http.post("./controladores/redesController.php", {accion:'consultar_redes'}).success(callback);
		}
	}
}])
.factory("direccionesFactory",['$http', function($http){
	return{
		asignar_idioma : function (idioma){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		asignar_id : function(valor_id){
			if(valor_id!="")
				id = valor_id;
			else
				id = "";
		},
		cargar_direcciones : function(callback){
			$http.post("./controladores/direccionController.php", {accion:'consultar_direccion_filtro',id_idioma:id_idioma}).success(callback);
		},
		cargar_mapas : function(callback){
			$http.post("./controladores/direccionController.php", {accion:'consultar_direcciones_mapas',id:id}).success(callback);
		}
	}
}])
.factory("direccionOrdenFactory",['$http', function($http){
	return{
		asignar_valor : function (idioma){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";

		},
		cargar_direccion_orden : function(callback){
			$http.post("./controladores/direccionController.php", {accion:'consultar_direccion_orden','id_idioma':id_idioma}).success(callback);
		}
	}
}])
.factory("tagsFactory",['$http', function($http){
	return{
		valor_id_idioma : function (idioma){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_tags : function(callback){
			$http.post("./controladores/tagsController.php", {accion:'consultar_tags2',id_idioma:id_idioma}).success(callback);
		}
	}
}]);
