angular.module("ContentManagerApp")
	.controller("GaleriaMultimediaConsultasController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,categoriasFactory,upload){
		//Cuerpo declaraciones
		$scope.menu = "activo";
		$scope.menu = "configuracion";
		$scope.titulo_pagina = "Galería Multimedia";
		$scope.subtitulo_pagina  = "Consultar galería";
		$scope.titulo_seccion  = "Visualizar imagenes según filtros";
		$scope.activo_img = "inactivo";
		$scope.galeria = {
							"id":"",
							"descripcion":"",
							"categoria":{
											"id":"",
											"descripcion":"",
											"estatus":""
							},
							"url":""
		}
		$scope.galeria_filtros = {
							"descripcion":"",
							"categoria":"",
		}
		$scope.titulo_registrar = "Registrar";
		$scope.categorias_menu = "1";
		$scope.base_url = $("#base_url").val();
		//--Cuerpo de metodos
		$scope.consultarGaleria = function(){
			galeriaFactory.asignar_valores("","",$scope.base_url)
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galeria=data;
				console.log($scope.galeria);
			});
		}
		$scope.consultarCategoriaIndividual = function(){
			categoriasFactory.asignar_valores("","",$scope.base_url)
			categoriasFactory.cargar_categorias(function(data){
				$scope.categorias=data;
				$scope.categorias_modal = data;
				console.log($scope.categorias);
			});
		}
		$scope.consultarFiltrosGaleria = function(){
			
			$scope.cero_registro = false

			if($scope.galeria_filtros.categoria==null){
				$categorias_id = "";
			}else{
				$categorias_id = $scope.galeria_filtros.categoria.id;
			}
			
			preloader_proceso("#mensaje_galeria",".contenedor_cards")

			galeriaFactory.asignar_valores($categorias_id,$scope.galeria_filtros.descripcion,$scope.base_url)
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galeria=data
				console.log($scope.galeria);
				if(isEmpty($scope.galeria)) {
					$scope.cero_registro = true
				}else{
					$scope.cero_registro = false
				}
			});
		}
		$scope.actualizarImagen = function(event){
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.tituloImagenModal = arreglo_atributos[0];
			$scope.categoriaImagenModal = arreglo_atributos[1];
			$scope.idImagenModal = arreglo_atributos[2];
			$("#categoriaImagenActualizar").val(arreglo_atributos[1]);
		}

		$scope.EliminarImagen = function(event){
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.tituloImagenModal = arreglo_atributos[0];
			$scope.categoriaImagenModal = arreglo_atributos[1];
			$scope.idImagenModal = arreglo_atributos[2];
			swal({
				  title: 'Esta seguro?',
				  text: "Desea eliminar este registro?",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si!',
				  cancelButtonText: 'No',
				}).then((result) => {
					  if (result.value) {
					  	$scope.procesarEliminar()
					  }
				})
		}

		$scope.procesarActualizarImagenModal = function(){
			if(($scope.tituloImagenModal!="")&&($scope.categoriaImagenModal!="")){
				$http.post($scope.base_url+"GaleriaMultimedia/actualizarImagen",{
					 'id':$scope.idImagenModal,
					 'id_categoria':$scope.categoriaImagenModal,	
					 'descripcion':$scope.tituloImagenModal, 	

				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					if($scope.mensajes.mensaje == "modificacion_procesada"){
						mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
						$("#cerrarModal").click();
					}else{
						mostrar_notificacion("Mensaje","Ocurrió un error inesperado","warning");
					}
				}).error(function(data,estatus){
					console.log(data);
				});
			}	
		}

		$scope.procesarEliminar = function (){
			$http.post($scope.base_url+"GaleriaMultimedia/eliminarImagen",
			{
				 'id':$scope.idImagenModal,	
				 'estatus':'2', 	
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "eliminacion_procesada"){
					Swal(
					      'Realizado!',
					      'El proceso fue ejecutado.',
					      'success'
				    ).then((result) => {
						  if (result.value) {
							   $scope.consultarFiltrosGaleria();
						  }
				    
					});
				}else{
					Swal(
					      'No realizado!',
					      'El proceso no pudo ser ejecutado.',
					      'warning'
				    )
				}
			}).error(function(data,estatus){
				console.log(data);
			});	
		}
		//--Cuerpo de llamados
		$scope.consultarGaleria();
		$scope.consultarCategoriaIndividual();
	});	
