angular.module("ContentManagerApp")
	.controller("mainController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,categoriasFactory){
		//Cuerpo declaraciones
		$scope.inicio = {
								'usuario':'',
								'clave':'',
		}

		$scope.base_url = $("#base_url").val();

		$scope.ingresar = function(){
			if($scope.validar_is()==true){
				uploader_reg("#mensaje_is","#usuario,#clave")
				$http.post($scope.base_url+"/Login/iniciarSession",
				{
					'usuario':$scope.inicio.usuario,
					'clave':$scope.inicio.clave,  	
				}).success(function(data, estatus, headers, config){
					$scope.mensajes  = data;
					if($scope.mensajes.mensajes == "inicio_exitoso"){
						$scope.ir_dashboard();
					}
					else{
						mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
						desbloquear_pantalla("#mensaje_is","#usuario,#clave")
					}

				}).error(function(data,estatus){
					console.log(data);
				});	
			}
		}
		//--
		$scope.validar_is =  function(){
			if($scope.inicio.usuario==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el nombre de usuario","warning");
				return false;
			}else if($scope.inicio.clave==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la clave","warning");
				return false;
			}
			else{
				return true;
			}
		}
		//--
		$scope.ir_dashboard = function(){
			let form = document.getElementById('loginform');
			form.action = "./inicio";
			form.submit();
		}
	});