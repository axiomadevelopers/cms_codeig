angular.module("ContentManagerApp")
	.controller("noticiasController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,noticiasFactory){
		//Cuerpo declaraciones
		$scope.menu = "activo";
		$scope.menu = "configuracion";
		$scope.titulo_pagina = "Noticias";
		$scope.subtitulo_pagina  = "Registrar noticias";
		$scope.activo_img = "inactivo";
		$scope.noticias = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'descripcion':'',
								'estatus':'',
								'id_imagen':''
		}
		$scope.id_noticias = ""
		$scope.searchNoticias = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_text = "Pulse aquí para ingresar el contenido  de la noticia"
		$scope.base_url = $("#base_url").val();

		//--------------------------------------------------------
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				//console.log($scope.idioma);
			});
		}
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.noticias.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.noticias.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.noticias.descripcion)
		}
		//--
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('4','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				console.log($scope.galery);
			});
		}

		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}

		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = $scope.base_url+vector_data[1];

			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.noticias.id_imagen = id_imagen
			$scope.noticias.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}

		$scope.registrarNoticias = function(){
			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.noticias.id!="")&&($scope.noticias.id!=undefined)){
					$scope.modificar_noticias();
				}else{
					$scope.insertar_noticias();
				}

			}
		}

		$scope.insertar_noticias = function(){
			$http.post($scope.base_url+"/Noticias/registrarNoticias",
			{
				'id':$scope.noticias.id,
				'titulo': $scope.noticias.titulo,
				'id_imagen':$scope.noticias.id_imagen,
				'descripcion':$scope.noticias.descripcion,
				'id_idioma':$scope.noticias.id_idioma.id,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_noticias();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.modificar_noticias = function(){
			$http.post($scope.base_url+"/Noticias/modificarNoticias",
			{
				'id':$scope.noticias.id,
				'titulo': $scope.noticias.titulo,
				'id_imagen':$scope.noticias.id_imagen,
				'descripcion':$scope.noticias.descripcion,
				'id_idioma':$scope.noticias.id_idioma,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.validar_form = function(){
			console.log($scope.noticias)
			if(($scope.noticias.id_idioma=="NULL")||($scope.noticias.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			} else if($scope.noticias.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if($scope.noticias.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}else if(($scope.noticias.id_imagen=="NULL")||($scope.noticias.id_imagen=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
				return false;
			}
			else{
				return true;
			}
		}

		$scope.limpiar_cajas_noticias = function(){
			$scope.noticias = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'descripcion':'',
								'estatus':'',
								'id_imagen':''
			}

			$scope.activo_img = "inactivo";

			$('#text_editor').data("wysihtml5").editor.clear();
			$("#div_descripcion").html($scope.titulo_text);
		}
		//--
		$scope.consultarNoticiasIndividual = function(){
			noticiasFactory.asignar_valores("",$scope.id_noticias,$scope.base_url)
			noticiasFactory.cargar_noticias(function(data){
				$scope.noticias=data[0];
				console.log(data[0]);
				$("#div_descripcion").html($scope.noticias.descripcion)
				$scope.borrar_imagen.push($scope.noticias.id_imagen);
				$scope.activo_img = "activo"
				$scope.noticias.imagen = base_url+"/"+$scope.noticias.ruta
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar categorías";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.noticias.id_idioma+'"]').prop('selected', true);
				},300);
			});
		}
		//---
		//--------------------------------------------------------
		//Cuerpo de llamados
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();
		//--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
		$scope.id_noticias  = $("#id_noticias").val();
		if($scope.id_noticias){
			$scope.consultarNoticiasIndividual();
		}
		//--------------------------------------------------------

	});
