angular.module("ContentManagerApp")
	.controller("SliderController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,noticiasFactory){
		//Cuerpo declaraciones
		$scope.menu = "activo";
		$scope.menu = "configuracion";
		$scope.titulo_pagina = "Slider";
		$scope.subtitulo_pagina  = "Registrar Slider";
		$scope.activo_img = "inactivo";

		$scope.slider = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'descripcion' : '',
						'boton' : '',
						'url' : '',
						'estatus' : '',
						'id_imagen' : '',
						'imagen' : ''
		}

		$scope.titulo_reg = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.searchSlider = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.base_url = $("#base_url").val();
		$scope.opcion = ''
		$scope.seccion = ''
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				//console.log($scope.idioma);
			});
		}
		//WISIMODAL
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.slider.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.slider.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.slider.descripcion)
		}
		//--
		$scope.agregar_contenido = function(){
			if ($scope.slider==undefined) {
					$scope.slider = {
									'id': '',
									'idioma': '',
									'id_idioma' : '',
									'titulo' : '',
									'descripcion' : '',
									'boton' : '',
									'url' : '',
									'estatus' : '',
									'id_imagen' : '',
									'imagen' : ''
					}
			}

		}
		//------------------------------------------CONSULTA DE IMAGEN SEGUN ID DE CATEGORIA
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('1','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				console.log($scope.galery);
			});
		}
		//MODAL DE IMG
		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}
		//PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
		$scope.seleccionar_imagen = function(event){
				var imagen = event.target.id;//Para capturar id
				console.log(imagen);
				var vec = $("#"+imagen).attr("data");
				var vector_data = vec.split("|")
				var id_imagen = vector_data[0];
				var ruta = vector_data[1];

				$scope.borrar_imagen.push(id_imagen);
				$scope.activo_img = "activo"
				$scope.slider.id_imagen = id_imagen
				$scope.slider.imagen = ruta
				//--
				$("#modal_img1").modal("hide");
				//--
		}

		$scope.consultar_idioma();
		$scope.consultar_galeria_img();

		$scope.id_slider = $("#id_slider").val();
			if($scope.id_slider){
				//$scope.consultarNosotrosIndividual();
			}
			//---------------------------------------------------------
	});
