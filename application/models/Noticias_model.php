<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Noticias_model extends CI_Model{

	public function iniciar_sesion($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' usuarios u');
		return $this->db->count_all_results();
	}

	public function guardarNoticias($data){
		if($this->db->insert("seccion_noticias",$data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarNoticias($data){
		if($data["id_noticias"]!=""){
			$this->db->where('a.id', $data["id_noticias"]);
		}
		$this->db->order_by('a.id');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen,d.login as usuario');
		$this->db->from('seccion_noticias a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        $this->db->join('galeria c', 'c.id = a.id_imagen');
        $this->db->join('usuarios d', 'd.id = a.id_usuario');
		$res = $this->db->get();
        
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function modificarNoticias($data){
		$this->db->where('id', $data["id"]);
        if($this->db->update("seccion_noticias", $data)){
        	return true;
        }else{
        	return false;
        }
	}

	public function consultarExisteTitulo($id,$titulo){
		$this->db->where('n.id !=',$id);
		$this->db->where('n.titulo',$titulo);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' seccion_noticias n');
		return $this->db->count_all_results();
	}

}