<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class GaleriaMultimedia_model extends CI_Model{

	public function guardarGaleria($data){
		if($this->db->insert("galeria", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarGaleria($data){
		if(isset($data["categoria"])){
			if($data["categoria"]!=""){
				$this->db->where('a.id_categoria', $data["categoria"]);
			}
		}	
		if(isset($data["nombre"])){
			if($data["nombre"]!=""){
				$this->db->where('a.titulo', $data["nombre"]);
			}
		}	
        $this->db->where('a.estatus!=',2);
        $this->db->order_by('a.id');
		$this->db->select('a.id,a.ruta,a.estatus,a.id_categoria,c.descripcion as descripcion_categoria');
		$this->db->from('galeria a');
		$this->db->join('categorias c', 'a.id_categoria = c.id');
		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function actualizarGaleria($data,$id){
		$this->db->where('id', $id);
        $this->db->update("galeria", $data);
        return true;
	}
}	