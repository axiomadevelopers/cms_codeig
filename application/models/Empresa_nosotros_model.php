<?php
	if (!defined('BASEPATH')) exit ('No direct script access allowed');

	class Empresa_nosotros_model extends CI_Model{

		public function iniciar_sesion($login,$clave){
			$this->db->where('login',$login);
			$this->db->where('clave',$clave);
			$this->db->where('estatus','1');
			$this->db->select('*');
			$this->db->from(' usuarios u');
			return $this->db->count_all_results();
		}

		public function guardar_nosotros($data){
			if($this->db->insert("empresa_nosotros",$data)){
				return true;
			}else{
				return false;
			}
		}

		public function consultarNosotros($data){
			if($data["id_nosotros"]!=""){
			$this->db->where('a.id', $data["id_nosotros"]);
			}
			$this->db->order_by('a.id');
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma,  c.ruta as ruta, c.id as id_imagen');
			$this->db->from('empresa_nosotros a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
			$this->db->join('galeria c', 'c.id = a.id_imagen');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function existe_nosotros($id){
			$this->db->where('id',$id);
			$this->db->from('empresa_nosotros a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function modificar_nosotros($data){
			$this->db->where('a.id', $data["id"]);
			$this->db->update('empresa_nosotros a', $data);
			return true;
		}

		public function modificarestatus($data){
			$this->db->where('id', $data["id"]);
			if($this->db->update("empresa_nosotros", $data)){
				return true;
			}else{
				return false;
			}
		}
	}
?>
