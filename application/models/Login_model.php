<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Login_model extends CI_Model{

	public function iniciar_sesion($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' usuarios u');
		return $this->db->count_all_results();
	}

	public function consultar_usuario($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('u.estatus','1');
		$this->db->select('u.*,b.nombres_apellidos');
		$this->db->from(' usuarios u');
		$this->db->join('personas b', 'b.id = u.id_persona');
		$res = $this->db->get();
		//print_r($this->db->last_query());die;
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
}