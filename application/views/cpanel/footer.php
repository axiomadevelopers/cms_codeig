</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
 </body>
 <!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="<?=base_url();?>/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?=base_url();?>/assets/plugins/popper/popper.min.js"></script>
<script src="<?=base_url();?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?=base_url();?>/assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?=base_url();?>/assets/js/waves.js"></script>
<!--Menu sidebar -->
<script src="<?=base_url();?>/assets/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="<?=base_url();?>/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<?=base_url();?>/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!--Custom JavaScript -->
<script src="<?=base_url();?>/assets/js/custom.min.js"></script>
<script src="<?=base_url();?>/assets/plugins/toast-master/js/jquery.toast.js"></script>
<script src="<?=base_url();?>/assets/js/toastr.js"></script>
<!-- ============================================================== -->
<script src="<?=base_url();?>/assets/plugins/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?=base_url();?>/assets/plugins/html5-editor/bootstrap-wysihtml5.js"></script>
<script>
    $(document).ready(function() {

        $('.textarea_editor').wysihtml5();


    });
    </script>
<!-- This page plugins -->
<!-- ============================================================== -->
<!-- chartist chart -->
<script src="<?=base_url();?>/assets/plugins/chartist-js/dist/chartist.min.js"></script>
<script src="<?=base_url();?>/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
<!--c3 JavaScript -->
<script src="<?=base_url();?>/assets/plugins/d3/d3.min.js"></script>
<script src="<?=base_url();?>/assets/plugins/c3-master/c3.min.js"></script>
<!-- Chart JS -->
<script src="<?=base_url();?>/assets/js/dashboard1.js"></script>
<!-- This is data table -->
<script src="<?=base_url();?>/assets/plugins/datatables/datatables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- ============================================================== -->
<!-- Sweet-Alert  -->
<script src="<?=base_url();?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?=base_url();?>assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
<!-- Dropzone Plugin JavaScript -->
<script src="<?=base_url();?>assets/plugins/dropzone-master/dist/dropzone.js"></script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="<?=base_url();?>assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
<script src="<?=base_url();?>assets/js/fbasic.js"></script>
<!-- CORE ANGULAR SCRIPTS -->
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/angular.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/angular-route.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/app.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/angular-sanitize.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/directivas/directives.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/servicios/services.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/mainController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/nosotrosController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/nosotrosConsultaController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/SliderController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/categoriasController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/categoriasConsultasController.js"></script>


<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/GaleriaMultimediaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/GaleriaMultimediaConsultasController.js"></script>


<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/noticiasController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/noticiasConsultasController.js"></script>

<!-- -->
</html>
