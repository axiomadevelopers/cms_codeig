<!DOCTYPE html>
<html lang="en" ng-app="ContentManagerApp">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url();?>/assets/images/logo.ico">
    <title>CMS Rocket </title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=base_url();?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?=base_url();?>/assets/plugins/html5-editor/bootstrap-wysihtml5.css"  rel="stylesheet"/>
    <!-- Dropzone css -->
    <link href="<?=base_url();?>/assets/plugins/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>/assets/plugins/datatables/media/css/dataTables.bootstrap4.css" rel="stylesheet">
    <!--alerts CSS -->
    <link href="<?=base_url();?>/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <!-- toast CSS -->
    <link href="<?=base_url();?>/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?=base_url();?>/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?=base_url();?>/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="<?=base_url();?>/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="<?=base_url();?>/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url();?>/assets/css/style.css" rel="stylesheet">
    <link href="<?=base_url();?>/assets/css/index.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?=base_url();?>/assets/css/colors/blue.css" id="theme" rel="stylesheet">
    <link href="<?=base_url();?>/assets/css/colors/green-dark.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
