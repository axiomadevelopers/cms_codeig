
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile" style="background: url(<?=base_url();?>assets/images/background/cms-info.jpg) no-repeat;">
            <!-- User profile image -->
            <div class="profile-img"> <img src="<?=base_url();?>assets/images/logo_peque.png" alt="user" /> </div>
            <!-- User profile text-->
            <div class="profile-text"> <a href="index.html#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" style="color: #fff!important;"
                    role="button" aria-haspopup="true" aria-expanded="true">Administrador</a>
                <div class="dropdown-menu animated flipInY"> <a href="index.html#" class="dropdown-item"><i class="ti-user"></i>
                        My Profile</a>

                <div class="dropdown-divider"></div> <a href="<?=base_url();?>/Login/logout" class="dropdown-item"><i class="fa fa-power-off"></i>
                        Logout</a>
                </div>
                <div style="display: none;" class="chartist-tooltip">
                </div>
            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">MENÚ</li>
                <li> <a class="has-arrow waves-effect waves-dark" aria-expanded="false"><i class="mdi mdi-bank"></i><span
                            class="hide-menu">Empresa </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?=base_url();?>Nosotros">Nosotros</a></li>
                        <li><a href="index2.html">Informacion</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?=base_url();?>Slider"><i class="mdi mdi-bank"></i>
                        <span class="hide-menu">Configurar Slider</span>
                    </a>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark"  aria-expanded="false"><i class="mdi mdi-cash"></i><span
                            class="hide-menu">Negocios </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="index.html">Tipo</a></li>
                        <li><a href="index2.html">Detalle</a></li>
                        <li><a href="index3.html">Ordenar</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?=base_url();?>noticias"><i class="mdi mdi-newspaper"></i>
                        <span class="hide-menu">Noticias </span>
                    </a>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark"  aria-expanded="false"><i class="mdi mdi-account-box"></i>
                        <spanclass="hide-menu">Contactos </span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="index.html">Contactos</a></li>
                        <li><a href="index2.html">Footer</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark"  aria-expanded="false"><i class="ti-settings"></i>

                <span class="" ng-class="{active:categorias_menu==='1',hide:categorias_menu===''}">Configuracion  {{categorias_menu}}</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?=base_url();?>categorias">Categoría</a></li>
                        <li><a href="<?=base_url();?>GaleriaMultimedia">Galería</a></li>
                        <li><a href="index3.html">Usuarios</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->
    <div class="sidebar-footer">
        <!-- item--><a href="index.html" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
        <!-- item-->
        <a href="index.html" class="link" data-toggle="tooltip" title="Settings"><i class="mdi mdi-comment-text"></i></a>
        <!-- item--><a href="index.html" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
    </div>
    <!-- End Bottom points-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
