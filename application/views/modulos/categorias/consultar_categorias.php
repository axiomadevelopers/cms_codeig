<div class="page-wrapper" >
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid" ng-controller="categoriasConsultasController">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Configuración</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Configuración</li>
                    <li class="breadcrumb-item active">Categorías</li>
                    <li class="breadcrumb-item active">Consultar</li>
                </ol>
            </div>
            <div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i
                                class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{titulo_pagina}}</h4>
                        <div class="table-responsive m-t-40">
                            <form id="formConsultaCategorias" method="POST" target="_self">
                                <input type="hidden" id="id_categoria" name="id_categoria" ng-model="id_categoria" >
                                 <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Descripción</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat = "cat in categorias track by $index">
                                            <td class="">{{cat.id}}</td>
                                            <td class="">{{cat.descripcion}}</td>
                                            <td class="centrado">
                                                <div class="form-group flotar_izquierda">
                                                    <div id="ver{{$index}}" ng-click="ver_categoria($index)" data="{{cat.id}}" class="btn btn-primary flotar_izquierda" title="modificar">
                                                        <i class="fas fa-pencil-alt" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_activar{{$index}}_a" name="btn_activar{{$index}}_a" class="btn btn-warning flotar_izquierda" ng-class="{visible:cat.estatus=='1',invisible:cat.estatus!=1}" title="Inactivar" data-ng-click="activar_registro($event)" data="{{cat.id}}|{{cat.estatus}}">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_activar{{$index}}_b" name="btn_activar{{$index}}_b"  class="btn btn-success flotar_izquierda" ng-class="{visible:cat.estatus!='1',invisible:cat.estatus==1}" title="Publicar" data-ng-click="activar_registro($event)" data="{{cat.id}}|{{cat.estatus}}">
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_eliminar{{$index}}" name="btn_eliminar{{$index}}"  class="btn btn-danger flotar_izquierda" title="Eliminar" data-ng-click="eliminar_registro($event)" data="{{cat.id}}|2">
                                                    <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </td>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div>
