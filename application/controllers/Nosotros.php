<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Nosotros extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Empresa_nosotros_model');
			$this->load->model('Idiomas_model');
		}

		public function index(){
			$this->load->view('cpanel/header');
			$this->load->view('cpanel/dashBoard');
			$this->load->view('cpanel/menu');
			$this->load->view('modulos/empresa/nosotros');
			$this->load->view('cpanel/footer');
		}

		public function consultar_nosotros(){
			$this->load->view('cpanel/header');
			$this->load->view('cpanel/dashBoard');
			$this->load->view('cpanel/menu');
			$this->load->view('modulos/empresa/consultarNosotros');
			$this->load->view('cpanel/footer');
		}

		function consultar_idioma(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Idiomas_model->consultarIdiomas($datos);
	        die(json_encode($respuesta));
		}

		public function insertarNosotros(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$data = array(
				'somos' => trim(mb_strtoupper($datos['somos'])),
				'digital_agency' => trim(mb_strtoupper($datos['digital_agency'])),
				'id_imagen' => $datos['id_imagen'],
		        'id_idioma' => $datos['id_idioma'],
		        'estatus' => '1'
        	);
			//print_r($data);
			$respuesta = $this->Empresa_nosotros_model->guardar_nosotros($data);
			if($respuesta==true){
            	$mensajes["mensaje"] = "registro_procesado";
	        }else{
	            $mensajes["mensaje"] = "no_registro";
	        }
	        die(json_encode($mensajes));
		}

		public function modificar_nosotros(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$data = array(
				'id' => $datos['id'],
				'somos' => trim(mb_strtoupper($datos['somos'])),
				'digital_agency' => trim(mb_strtoupper($datos['digital_agency'])),
				'id_imagen' => $datos['id_imagen'],
		        'id_idioma' => $datos['id_idioma'],
		        'estatus' => '1'
        	);
			//print_r($data);die;
			$existe = $this->Empresa_nosotros_model->existe_nosotros($data['id']);
			//print_r ($existe);die;
			if ($existe == false) {
				$mensajes["mensaje"] = "no_existe";
			}else{
				$respuesta = $this->Empresa_nosotros_model->modificar_nosotros($data);
				//print_r ($respuesta);die;
				if($respuesta!=false){
	            	$mensajes["mensaje"] = "registro_procesado";
		        }else{
		            $mensajes["mensaje"] = "no_registro";
		        }
		        die(json_encode($mensajes));
			}
		}

		public function consultarNosotrosTodos(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$respuesta = $this->Empresa_nosotros_model->consultarNosotros($datos);
			foreach ($respuesta as $key => $value) {
	            $valor[] = array(
								"id" => $value->id,
								"id_idioma" => $value->id_idioma,
								"somos" => $value->somos,
								"digital_agency" => $value->digital_agency,
								"id_imagen" => $value->id_imagen,
								"estatus" => $value->estatus,
								"descripcion_idioma" => $value->descripcion_idioma,
								"ruta" => $value->ruta,
								"somos1" => strip_tags($value->somos),
								"digital_agency1" => strip_tags($value->digital_agency)
				);
	        }
	        $listado = (object)$valor;
	        die(json_encode($listado));
		}

		public function nosotrosVer(){
			$datos["id"] = $this->input->post('id_nosotros');
			$this->load->view('cpanel/header');
			$this->load->view('cpanel/dashBoard');
			$this->load->view('cpanel/menu');
			$this->load->view('modulos/empresa/nosotros', $datos);
			$this->load->view('cpanel/footer');
		}

		public function modificarnosotrosEstatus(){
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $data = array(
	          'id' =>$datos['id'],
	          'estatus' => $datos['estatus'],
	        );
	        $respuesta = $this->Empresa_nosotros_model->modificarestatus($data);

	        if($respuesta==true){
	            $mensajes["mensaje"] = "modificacion_procesada";
	        }else{
	            $mensajes["mensaje"] = "no_modifico";
	        }

	        die(json_encode($mensajes));
	    }
	}
?>
