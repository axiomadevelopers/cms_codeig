<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class GaleriaMultimedia extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('GaleriaMultimedia_model');
      $this->load->model('Categorias_model');
      if (!$this->session->userdata("login")) {
        redirect(base_url());
      }
    }

    public function index(){
      $this->load->view('cpanel/header');
      $this->load->view('cpanel/dashBoard');
      $this->load->view('cpanel/menu');
      $this->load->view('modulos/galeria/galeria_multimedia');
      $this->load->view('cpanel/footer');
    }
    
    public function upload(){

        /*-----------------------------------------------*/
        /*var_dump($this->input->post());
        var_dump($_FILES["file"]);
        die('');*/
        if($this->input->post()){
            //---------------------------------------------
            $file = $_FILES["file"]["name"];
            $filetype = $_FILES["file"]["type"];
            $filesize = $_FILES["file"]["size"];
            $id_categoria = $_POST["categoria"];
            $tipo = explode("/", $filetype);
            $nombre_archivo = $this->input->post("nombre_archivo").".".$tipo[1];
            $data["id_categoria"] = $this->input->post("categoria");
            $rs_categorias = $this->Categorias_model->consultarCategoria($data);
            $categoria = strtolower($rs_categorias[0]->descripcion);
            $ruta ="assets/images/archivos/".$categoria."/";
            if(!is_dir("assets/images/archivos/".$categoria."/")){
                mkdir($ruta,0777);
            } 
            $config['upload_path']          = $ruta;
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 100;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $config['file_name'] = $nombre_archivo;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('file')){
                $error = array('error' => $this->upload->display_errors());
                die(json_encode($error));
            }
            else{
                $data_img = array('upload_data' => $this->upload->data());
                $archivo1 = str_replace(" ","_",$nombre_archivo);
                $data2 = array(
                  'ruta' => $ruta.$archivo1,
                  'id_categoria' => $this->input->post("categoria"),
                  'titulo' => $this->input->post("nombre_archivo"),
                  'estatus' => '1'
                );
                $respuesta = $this->GaleriaMultimedia_model->guardarGaleria($data2);
                
                if($respuesta==true){
                    $mensajes["mensaje"] = "registro_procesado";
                }else{
                    $mensajes["mensaje"] = "error";
                }
                
                die(json_encode($mensajes));
            }    
        //---------------------------------------------
        }
        /*-----------------------------------------------*/
    }
    
    public function consultarGaleria(){
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard');
        $this->load->view('cpanel/menu');
        $this->load->view('modulos/galeria/consultar_galeria_multimedia');
        $this->load->view('cpanel/footer');
    }

    public function consultarGaleriaCategoria(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        
        $respuesta = $this->GaleriaMultimedia_model->consultarGaleria($datos);
        $listado = [];
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $vector_url = explode("/",$value->ruta);
            $vector_url2 = explode(".",$vector_url[4]);
            $valor->titulo = strtoupper($vector_url2[0]);
            $listado[]=$valor;
        }
        $listado2 = $listado;
        die(json_encode($listado2));
    }

    public function actualizarImagen(){

        $datos= json_decode(file_get_contents('php://input'), TRUE);

        $data2 = array(
                  'titulo' => $datos["descripcion"],
        );

        $respuesta = $this->GaleriaMultimedia_model->actualizarGaleria($data2,$datos["id"]);

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
        }else{
            $mensajes["mensaje"] = "error";
        }
        
        die(json_encode($mensajes));
    }

    public function eliminarImagen(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);

        $data2 = array(
                  'estatus' => $datos["estatus"],
        );

        $respuesta = $this->GaleriaMultimedia_model->actualizarGaleria($data2,$datos["id"]);

        if($respuesta==true){
            $mensajes["mensaje"] = "eliminacion_procesada";
        }else{
            $mensajes["mensaje"] = "error";
        }
        
        die(json_encode($mensajes));
    }
}    