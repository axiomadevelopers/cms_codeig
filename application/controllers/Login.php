<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('Login_model');
    }

    public function index(){
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoardLogin');
        $this->load->view('modulos/login/login');
        $this->load->view('cpanel/footer');
    }

    public function iniciarSession(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        #Validar campos
        if(($datos["usuario"])&&($datos["clave"])){
            $recordset = $this->Login_model->iniciar_sesion($datos["usuario"],sha1($datos["clave"]));
            if($recordset > 0){
                #Consulto al usuario
                $record_usuario = $this->Login_model->consultar_usuario($datos["usuario"],sha1($datos["clave"]));
                $mensajes["mensajes"] = "inicio_exitoso";
                $data = array(
                    'login' => $datos["usuario"],
                    'activo' => true,
                    'id' => $record_usuario[0]->id_persona,
                    'nombre_persona' => $record_usuario[0]->nombres_apellidos
                );
                $this->session->set_userdata($data);
            }else{
                $mensajes["mensajes"] = "error_datos";
            }
            die(json_encode($mensajes));      
        }
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }    
}  