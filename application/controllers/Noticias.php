<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Noticias extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('Noticias_model');
      if (!$this->session->userdata("login")) {
        redirect(base_url());
      }
    }

    public function index(){
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard');
        $this->load->view('cpanel/menu');
        $this->load->view('modulos/noticias/noticias');
        $this->load->view('cpanel/footer');
    }

    public function registrarNoticias(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'titulo' => trim(mb_strtoupper($datos['titulo'])),
          'id_imagen' => $datos['id_imagen'],
          'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
          'estatus' => '1',
          'fecha' => date("Y-m-d"),
          'id_usuario' => $this->session->userdata("id"),
          'slug' =>$this->generarSlug($datos["titulo"]),
          'id_idioma' => $datos['id_idioma']
        );
        $respuesta = $this->Noticias_model->guardarNoticias($data);
        if($respuesta==true){
            $mensajes["mensaje"] = "registro_procesado";
        }else{
            $mensajes["mensaje"] = "no_registro";
        }
        die(json_encode($mensajes));
    }

    public function modificarNoticias(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        //-Verifico si existe una noticia con ese titulo....
        $existe_titulo = $this->Noticias_model->consultarExisteTitulo($datos["id"],$datos["titulo"]);
        if($existe_titulo==0){
            $data = array(
              'id' =>  $datos['id'],
              'titulo' => trim(mb_strtoupper($datos['titulo'])),
              'id_imagen' => $datos['id_imagen'],
              'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
              'estatus' => '1',
              'fecha' => date("Y-m-d"),
              'slug' =>$this->generarSlug($datos["titulo"]),
              'id_idioma' => $datos['id_idioma']
            );
            $respuesta = $this->Noticias_model->modificarNoticias($data);
            if($respuesta==true){
                $mensajes["mensaje"] = "registro_procesado";
            }else{
                $mensajes["mensaje"] = "no_registro";
            }
        }else{
             $mensajes["mensaje"] = "existe";
        }
        //--
        die(json_encode($mensajes));
    }

    public function consultarNoticias(){
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard');
        $this->load->view('cpanel/menu');
        $this->load->view('modulos/noticias/consultar_noticias');
        $this->load->view('cpanel/footer');
    }

    public function consultarNoticiasTodas(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->Noticias_model->consultarNoticias($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $valor->descripcion_sin_html = strip_tags($value->descripcion);
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function generarSlug($cadena){
        $titulo_min = strtolower($this->normaliza($cadena));
        $slug_noticias = str_replace(" ","-",$titulo_min);
        $slug_noticias = preg_replace("/[^a-zA-Z0-9_-]+/", "", $slug_noticias);
        return $slug_noticias;
    }

    public function normaliza ($cadena){
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtolower($cadena);
        return utf8_encode($cadena);
    }

    public function modificarNoticiasEstatus(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'id' =>$datos['id'],  
          'estatus' => $datos['estatus'],
        );
        $respuesta = $this->Noticias_model->modificarNoticias($data);

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
        }else{
            $mensajes["mensaje"] = "no_modifico";
        }  
       
        die(json_encode($mensajes));
    }

    public function noticiasVer(){
        $datos["id"] = $this->input->post('id_noticias');
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard');
        $this->load->view('cpanel/menu');
        $this->load->view('modulos/noticias/noticias',$datos);
        $this->load->view('cpanel/footer');
    }
}    
