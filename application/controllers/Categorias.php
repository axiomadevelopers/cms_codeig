<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Categorias extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('Categorias_model');
      if (!$this->session->userdata("login")) {
        redirect(base_url());
      }
    }

    public function index(){
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard');
        $this->load->view('cpanel/menu');
        $this->load->view('modulos/categorias/categorias');
        $this->load->view('cpanel/footer');
    }

    public function consultarCategorias(){
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard');
        $this->load->view('cpanel/menu');
        $this->load->view('modulos/categorias/consultar_categorias');
        $this->load->view('cpanel/footer');
    }

    public function registrarCategorias(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
          'estatus' => '1'
        );
        $respuesta = $this->Categorias_model->guardarCategoria($data);
        if($respuesta==true){
            $mensajes["mensaje"] = "registro_procesado";
        }else{
            $mensajes["mensaje"] = "no_registro";
        }
        die(json_encode($mensajes));
    }

    public function consultarCategoriasTodas(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->Categorias_model->consultarCategoria($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $valor->descripcion = mb_strtoupper($value->descripcion);
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }

    public function categoriasVer(){
        $datos["id"] = $this->input->post('id_categoria');
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard');
        $this->load->view('cpanel/menu');
        $this->load->view('modulos/categorias/categorias',$datos);
        $this->load->view('cpanel/footer');
    }

    public function modificarCategorias(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'id' =>$datos['id'],  
          'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
        );
        //verifico si existe la categoria con ese nombre
        $existe = $this->Categorias_model->existeCategoria("",$data['descripcion']);
        if(count($existe)>0){
            if($existe[0]->id != $data["id"]){
                $mensajes["mensaje"] = "existe_nombre";
                die(json_encode($mensajes));
            }else
                $respuesta = $this->Categorias_model->modificarCategoria($data);
        }else{
            $respuesta = $this->Categorias_model->modificarCategoria($data); 
        }

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
        }else{
            $mensajes["mensaje"] = "no_modifico";
        }  
       
        die(json_encode($mensajes));
    }

    public function modificarCategoriasEstatus(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'id' =>$datos['id'],  
          'estatus' => $datos['estatus'],
        );
        $respuesta = $this->Categorias_model->modificarCategoria($data);

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
        }else{
            $mensajes["mensaje"] = "no_modifico";
        }  
       
        die(json_encode($mensajes));
    }
}  