<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Inicio extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('inicio_model');
      if (!$this->session->userdata("login")) {
        redirect(base_url());
      }
    }

    public function index(){
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard');
        $this->load->view('cpanel/menu');
        $this->load->view('inicio');
        $this->load->view('cpanel/footer');
    }
}  